import React from 'react'
import Layout from './src/components/layout'
// custom typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

export const wrapPageElement = ({ element, props }) => {
  // props provide same data to Layout as Page element will get
  // including location, data, etc - you don't need to pass it
  if (!props.data) return element
  return <Layout {...props}>{element}</Layout>
}
