const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')
const { transformGhostPath, getTagSlug } = require('./src/utils/pathname')

exports.createPages = ({ graphql, actions }) => {
  const { createPage, createRedirect } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const blogPostAmp = path.resolve(`./src/templates/blog-post.amp.js`)
  const blogPostList = path.resolve('./src/templates/blog-post-list.js')
  const tagPostList = path.resolve(`./src/templates/tag-post-list.js`)

  return Promise.all([
    graphql(
      `
        {
          allMarkdownRemark(
            sort: { fields: [frontmatter___date], order: DESC }
            limit: 1000
          ) {
            edges {
              node {
                fields {
                  slug
                }
                frontmatter {
                  title
                  slug
                  tags
                }
              }
            }
          }
        }
      `
    ).then(result => {
      if (result.errors) {
        throw result.errors
      }

      createRedirect({
        fromPath: '/index.html',
        redirectInBrowser: true,
        toPath: '/',
      });

      const posts = result.data.allMarkdownRemark.edges
      const postsPerPage = 6
      const numPages = Math.ceil(posts.length / postsPerPage)

      // Create pagination.
      Array.from({ length: numPages }).forEach((_, page) => {
        const path = (() => {
          if (page === 0) {
            return '/'
          } else if (page < 0 || page >= numPages) {
            return ''
          } else {
            return `/page/${page + 1}`
          }
        })()
        createPage({
          path,
          component: blogPostList,
          context: {
            totatPosts: posts.length,
            limit: postsPerPage,
            skip: page * postsPerPage,
            numPages,
            currentPage: page + 1,
          },
        })
      })
      createRedirect({
        fromPath: '/page/1',
        redirectInBrowser: true,
        toPath: '/',
      });
      
      // Create blog posts pages.
      posts.forEach((post, index) => {
        const previous = index === posts.length - 1 ? null : posts[index + 1].node.fields.slug
        const next = index === 0 ? null : posts[index - 1].node.fields.slug
        createPage({
          path: transformGhostPath(post.node.fields.slug),
          component: blogPost,
          context: {
            slug: post.node.fields.slug,
            previous,
            next,
          },
        })

        createPage({
          path: `${transformGhostPath(post.node.fields.slug)}amp`,
          component: blogPostAmp,
          context: {
            slug: post.node.fields.slug,
          },
        })
      })

      // Create tag pages
      const tags = [...new Set([].concat.apply([], posts.map(post => post.node.frontmatter.tags)))]
      // tags.forEach(tag => {
      //   createPage({
      //     path: `/tag/${getTagSlug(tag)}`,
      //     component: tagPostList,
      //     context: {
      //       tag,
      //     },
      //   })
      // })

      // Create tag pagination
      tags.forEach(tag => {
        const tagPosts = posts.filter(post => post.node.frontmatter.tags.includes(tag))
        const tagNumPages = Math.ceil(tagPosts.length / postsPerPage)
        Array.from({ length: tagNumPages }).forEach((_, page) => {
          const path = (() => {
            if (page === 0) {
              return `/tag/${getTagSlug(tag)}`
            } else if (page < 0 || page >= numPages) {
              return ''
            } else {
              return `/tag/${getTagSlug(tag)}/page/${page + 1}`
            }
          })()
          createPage({
            path,
            component: tagPostList,
            context: {
              tag,
              totatPosts: tagPosts.length,
              limit: postsPerPage,
              skip: page * postsPerPage,
              numPages: tagNumPages,
              currentPage: page + 1,
            },
          })
        })
        createRedirect({
          fromPath: `/tag/${getTagSlug(tag)}/page/1`,
          redirectInBrowser: true,
          toPath: `/tag/${getTagSlug(tag)}`,
        })
      })
      return null
    }),
    // graphql(
    //   `
    //     {
    //       allMarkdownRemark(
    //         limit: 6
    //         filter: {fileAbsolutePath: {regex: "/blog/"}}
    //         sort: {fields: [fields___date], order: DESC}
    //       ) {
    //         edges {
    //           node {
    //             fields {
    //               title
    //               slug
    //               slug
    //             }
    //           }
    //         }
    //       }
    //     }
    //   `,
    // ).then(result => {

    // })
  ])
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
