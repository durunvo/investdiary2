---
title: "ประเมินมูลค่าบทที่ 3 - Risk Free Rate คืออะไร?"
slug: "/aswath-damodaran-3-risk-free-rate"
date: "2017-06-16T05:51:55.000Z"
description: "Risk Free Rate คือผลตอบแทนแบบที่ไม่มีความเสี่ยงในการลงทุน แปลว่าผู้ที่ออกหลักทรัพย์ไม่มีความเสี่ยงในการผิดนัดชำระ"
image: "./images/15401031_878102505658331_1107596378148981524_n_nd6ynh.png"
featured: false
draft: false
tags: ["แนวคิดการลงทุน","Aswath Damodaran","การประเมินมูลค่า"]
---

**Risk Free Rate** คือผลตอบแทนแบบที่ไม่มีความเสี่ยงในการลงทุน แปลว่าผู้ที่ออกหลักทรัพย์ไม่มีความเสี่ยงในการผิดนัดชำระ (Default risk) เช่นพันธบัตรรัฐบาล ซึ่งสำคัญใน[การประเมินมูลค่าพื้นฐาน](https://investdiary.co/2017/06/16/aswath-damodaran-2-intrinsic-valuation/ "การประเมินมูลค่าพื้นฐาน")

**Discounted Rate** นั้นสำคัญสำหรับการหา Discounted Cash Flow Valuation (DCF) แต่การคาดการณ์ Cashflow ก็สำคัญเช่นกัน

Video ตัวเต็ม: https://goo.gl/jSsxE4

มาดูพื้นฐาน Discounted Rate

กระแสเงินสดของส่วนของเงินทุน - Discounted Rate ควรจะเป็นต้นทุนส่วนเงินทุน

กระแสเงินสดของทั้งกิจการ - Discounted Rate ควรจะเป็นต้นทุนทางการเงินทั้งหมด (เงินกู้และเงินทุน)

กระแสเงินสดปรับค่าเงินเฟ้อ - Discounted Rate ก็ต้องปรับค่าเงินเฟ้อ

ยิ่งหลักทรัพย์มีความเสี่ยงมากยิ่งมีต้นทุนของเงินทุนมาก

ความเสี่ยงวัดยังไง?

ให้ลองดูจากนักลงทุนเจ้าใหญ่ๆที่กระจายความเสี่ยงดีแล้วมองความเสี่ยงยังไง

**Risk Free Rate** จะต้องมี 2 อย่าง

1. ไม่มีความเสี่ยงในการผิดนัดชำระ (Default Risk)
2. ไม่มีความเสี่ยงจากการเปลี่ยนแปลงของอัตราดอกเบี้ยในการลงทุนต่อ (Reinvestment)

Risk Free Rate มีข้อสังเกตุที่ควรรู้

1. Risk free rate จะต่างกันหากเวลาในการลงทุนต่างกัน เช่นพันธบัตร 3 เดือน จะมี risk free rate ต่างกัน พันธบัตร 10 ปี
2. Risk free rate แต่ละประเทศไม่เหมือนกัน
3. บางรัฐบาลก็จะมีความเสี่ยงในการผิดชำระ พันธบัตรที่ออกมาก็จะมีความเสี่ยง

###หา Risk Free Rate ในค่าเงิน USD?

เราสามารถเลือกพันธบัตรรัฐบาลแบบ 3 เดือน หรือ 6 เดือน หรือ 10 ปีก็ได้ ขึ้นอยู่กับว่าเรากำลังมองหาการลงทุนแบบระยะยาวหรือสั้น ถ้ายาวก็อาจจะเลือกพันธบัตรแบบ 10 ปี Risk Free Rate ก็จะเป็นผลตอบแทนของพันธบัตรนั้น

###หา Risk Free Rate ในค่าเงิน EURO?

เราก็เอาแต่ละประเทศในยุโรปมาดูว่าผลตอบแทนของพันธบัตรในแต่ละประเทศเป็นเท่าไหร่ Risk Free Rate ก็จะเป็นผลตอบแทนของพันธบัตรของประเทศที่ให้ผลตอบแทนต่ำที่สุด ในเคสนี้คือเยอรมัน

ถ้าจะหา Risk Free Rate ในต่างประเทศ ก็ต้องหาส่วนต่างเรตติ้งระหว่างประเทศที่เราจะดูกับประเทศที่เราเอาเป็นบรรทัดฐาน แล้วบวกหรือลบ ขึ้นอยู่กับว่าเรตติ้งมากกว่าหรือน้อยกว่า

สรุป

ส่วนแรกที่จะนำไปคาดการณ์ Discounted Rate นั้นก็คือ Risk Free Rate

เงินบางสกุลนั้นอาจจะหา Risk Free Rate ได้ง่ายกว่า เราต้องหามาให้ได้ก่อนที่จะทำอย่างอื่น

ปล. จริงๆแล้วมันละเอียดกว่านี้มาก แต่เท่าที่ผมลองฟังดู สำหรับนักลงทุนเราแค่รู้ Concept เท่านี้ก็น่าจะเพียงพอแล้ว

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
