---
title: "ส่องบริษัทไหนจะเจ๊งไม่เจ๊งผ่าน F-Score"
slug: "/132"
date: "2018-05-02T07:16:00.000Z"
description: "เราจะรู้ได้อย่างไรว่าบริษัทไหนมีงบการเงินแข็งแกร่ง หากเราลงทุนไปแล้วบริษัทจะไม่ล้มละลาย?"
image: "./images/bankrupt-2922154_1920_1_-min_owiaww.jpg"
featured: false
draft: false
tags: ["การลงทุน","แนวคิดการลงทุน","การประเมินมูลค่า","EARTH","POLAR","TSLA","EFORL","BCP"]
---

เราจะรู้ได้อย่างไรว่าบริษัทไหนมีงบการเงินแข็งแกร่ง หากเราลงทุนไปแล้วบริษัทจะไม่ล้มละลาย?

การล้มละลายหรือการถูกเพิกถอนจากตลาดของบริษัทถือเป็นความเสี่ยงที่ใหญ่หลวงมากที่สุดสำหรับนักลงทุน

หากเราลงทุนในบริษัทแล้วเกิดเหตุการณ์ข้างต้น พอร์ทการลงทุนของเราก็อาจจะเกิดความเสียหายอย่างหนักได้ง่ายๆ

ดังนั้น การตรวจสอบความแข็งแกร่งของบริษัทก่อนลงทุนจึงเป็นสิ่งแรกๆที่นักลงทุนทุกคนควรให้ความสนใจ

นักลงทุนทั่วไปสามารถตรวจสอบฐานะการเงินของบริษัทผ่านงบการเงินประจำปี หรือประจำไตรมาสได้

จริงๆแล้วหลักการที่สำคัญนั้นไม่ยากเลย "บริษัทที่จะไม่มีปัญหา คือบริษัทที่ไม่มีหนี้"

หากเราลงทุนในบริษัทที่ไม่มีหนี้สินเลยหรือมีน้อย เราก็มั่นใจได้ในระดับสูงว่าบริษัทจะไม่เกิดปัญหาทางการเงิน

แต่บริษัทส่วนใหญ่ต่างมีหนี้สินเพื่อทำธุรกิจ ดังนั้นทุกบริษัทต่างก็มีความเสี่ยงด้วยกันทั้งสิ้น คำถามคือเราจะประเมินอย่างไร?

.

F-Score ช่วยเราได้

F-Score เป็นเครื่องมือการจัดลำดับความมั่นคงทางการเงิน โดย Mr.Joseph D. Piotroski

อย่าพึ่งถอนหายใจว่าจะเจอสูตรคำนวนล่ำลึกพิศดาร เพราะจริงๆแล้วมันไม่ใช่สูตรซักทีเดียว แต่เป็นวิธีการประเมินบริษัทมากกว่า

F-Score จะประกอบไปด้วยคำถาม 9 ข้อสำหรับบริษัทที่เราจะประเมิน โดยแต่ละข้อจะสามารถตอบได้เพียง 2 คำตอบด้วยกันคือ "ใช่" จะได้ 1 คะแนน และ "ไม่ใช่" จะได้ 0 คะแนน

คำถามก็คือ

1. ROA มากกว่า 0 หรือไม่?
2. ROA ดีขึ้นจากปีก่อนหรือไม่?
3. CFO มากกว่า 0 หรือไม่?
4. CFO มากกว่ากำไรสุทธิหรือไม่?
5. D/E ลดลงจากปีก่อนหรือไม่?
6. Current Ratio ดีขึ้นจากปีก่อนหรือไม่?
7. ไม่มีการเพิ่มทุน
8. อัตรากำไรขั้นต้นดีขึ้นจากปีก่อนหรือไม่?
9. Asset turnover ดีขึ้นจากปีก่อนหรือไม่?

เมื่อตอบได้ครบทุกข้อแล้ว เราจะรวมคะแนนทั้งหมดของบริษัท หากรวมแล้วได้ 0 คะแนนหมายถึง "แย่มาก" และ 9 คะแนนคือ "เยี่ยมมาก"

จริงๆแล้ว F-Score อาจจะดูซับซ้อน และคำถามแต่ละข้อก็อาจจะต้องใช้ความรู้ระดับหนึ่งถึงจะค้นหาคำตอบได้

แต่สำหรับมือใหม่ขอให้เข้าใจเพียงแค่ว่า ความสำคัญก็คือ "บริษัทจะต้องมีฐานะการเงินดี" "มีผลประกอบการที่ดี" และที่สำคัญ "บริษัทจะต้องมีกระแสเงินสดที่ดี"

.

F-Score เชื่อถือได้มากน้อยแค่ไหน? เราลองมาดูบริษัทจริงๆดูกันบ้าง

บริษัท EFORL ได้ F-Score เพียง 2 คะแนนเท่านั้น หมายความว่า EFORL เป็นบริษัทที่มีโอกาสล้มละลายสูง

ซึ่งล่าสุด EFORL ก็มีการเพิ่มทุนให้ คุณวิชัย ทองแตง ได้เข้าถือหุ้นใหญ่เมื่อไม่นานมานี้ ทำให้ EFORL สามารถรอดพ้นจะวิกฤตได้ชั่วคราว

บริษัทต่อมาคือ EARTH ได้ F-Score เพียง 3 คะแนนเท่านั้น แปลว่า EARTH เองก็มีฐานะการเงินที่ย่ำแย่เช่นกัน และในปีก่อนเราก็รู้กันไปแล้วว่า EARTH ได้ถูกเพิกถอนออกจากตลาดหุ้นเรียบร้อยแล้ว

เราลองมาดูอีกตัวอย่างบริษัทที่ย่ำแย่กันบ้าง POLAR เป็นบริษัทที่น่าทดสอบ F-Score อย่างยิ่ง

POLAR ได้ F-Score เพียง 3 คะแนนเท่านั้น และท้ายที่สุดก็เป็นกรณีเดียวกันกับ EARTH นั้นก็คือการถูกถอนออกจากตลาด

เราอาจจะสามารถสรุปได้ว่าบริษัทที่มี F-Score ต่ำกว่า 4 คะแนนถือว่ามีความเสี่ยง นักลงทุนที่ไม่ได้ลงลึกมากควรจะหลีกเลี่ยงบริษัทประเภทนี้ทุกประการ

งั้นเราลองมาดูบริษัทกลางๆกันบ้างว่ามี F-Score เป็นเท่าไหร่

บริษัท BCP มี F-Score อยู่ที่ 7 คะแนน แปลว่าบริษัทนี้หายห่วงเรื่องปัญหาทางการเงินและความเสี่ยงในการลงทุน

เคสสุดท้ายเราลองมาดูบริษัทที่น่าสนใจอีกหนึ่งบริษัทกัน นั่นคือ TESLA ผู้ผลิตรถยนต์ EV รายใหญ่ของพี่ Elon Musk กัน

TESLA มี F-Score ที่ต่ำมากเพียง 2 คะแนนเท่านั้น หมายความว่าจริงๆแล้วบริษัทนี้มีปัญหาทางการเงินอย่างมาก

แต่เหตุใดบริษัทจึงยังสามารถอยู่รอดได้และไม่ล้มละลาย?

คำตอบก็คือ เพราะนักลงทุนยังเชื่อมั่น Elon Musk และยอมเพิ่มทุนให้ทุกปี หรือพูดง่ายๆคือ "บริษัทยังอยู่ได้เพราะเงินนักลงทุน"

อนาคตไม่มีใครรู้ว่า Tesla จะประสบความสำเร็จหรือล้มละลาย สำหรับนักลงทุนที่ชื่นชอบการลงทุนในความฝันก็อาจจะรอลุ้นได้

แต่สำหรับนักลงทุนที่ไม่ต้องการให้เงินทุนของเราเกิดความเสี่ยง Tesla ก็อาจจะเป็นอีกบริษัทที่เราควรจะหลีกเลี่ยงเช่นกัน

.

สรุปคือ F-Score ถือว่าเป็นเครื่องมือในการประเมินการเงินของบริษัทที่มีประสิทธิภาพอย่างมาก

แก่นหลักของ F-Score คือพยายามดูฐานะการเงินและผลประกอบการของบริษัทในจุดสำคัญต่าง และยังดูย้อนกลับว่ามีพัฒนาการที่ดีขึ้นหรือแย่ลง

บริษัทที่มี F-Score ต่ำกว่า 4 ถือว่ามีความเสี่ยงสูงและควรหลีกเลี่ยง เพราะโอกาสที่บริษัทเหล่านี้จะถูกเพิกถอนมีสูง

นักลงทุนไม่จำเป็นต้องนำเงินไปเสี่ยงกับบริษัทเหล่านี้ เพราะในตลาดหุ้นยังมีหุ้นดีๆอีกมากมายที่สามารถให้ผลตอบแทนดีและความเสี่ยงยังต่ำอีกด้วย

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)


