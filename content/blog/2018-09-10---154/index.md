---
title: "แนวโน้มการท่องเที่ยวกับหุ้นสายการบิน?"
slug: "/154"
date: "2018-09-10T14:38:28.000Z"
description: "หลายคนคงจะรู้แล้วว่าในช่วงปีมานี้ Warren Buffett ได้เข้าลงทุนในบริษัทสายการบินอย่าง Delta และ Southwest airline"
image: "./images/A330neo-first-flight-in-flight-039_copy_1_etvanw.jpg"
featured: false
draft: false
tags: ["aav","การลงทุน","thai","nok","การท่องเที่ยว","ba"]
---

หลายคนคงจะรู้แล้วว่าในช่วงปีมานี้ Warren Buffett ได้เข้าลงทุนในบริษัทสายการบินอย่าง Delta และ Southwest airline

ซึ่งสามารถสรุปได้คร่าวๆว่า จริงๆแล้วหุ้นสายการบินก็ไม่ได้แย่เสมอไป

ความน่าสนใจคือในประเทศไทย หุ้นสายการบินมีความน่าสนใจในการลงทุนมากน้อยหรือไม่

อย่างที่ทุกคนรู้ดีกันว่าแนวโน้มการท่องเที่ยวของโลกนี้กำลังเติบโตขึ้นเรื่อยๆ

และหากเจาะมาดูเฉพาะการท่องเที่ยวของคนไทยก็จะพบว่าเติบโตขึ้นมาก ไม่ว่าจะเป็นภายในหรือต่างประเทศก็ได้

การท่องเที่ยวที่เติบโตขึ้นส่งผลดีต่อสายการบินรวมถึงบริษัทที่เกี่ยวข้องอย่างมาก เพราะจำนวนคนเที่ยวที่เพิ่มมากขึ้นก็หมายถึงตลาดที่เติบโตขึ้น

หลักฐานที่ช่วยยืนยันการเติบโตขึ้นนี้อย่างดีก็มี ทั้งโครงการขยายสนามบิน จำนวนผู้นักท่องเที่ยวต่อปีที่เพิ่มสูงขึ้น จำนวนเที่ยวบินที่เพิ่มสูงขึ้น เป็นต้น

และหากเจาะลึกลงก็จะพบว่าแนวโน้มสายการบิน Low cost มีการเติบโตสูงกว่าสายการบินแบบดั่งเดิม

หากมองคร่าวๆแล้วจะพบว่าบริษัทสายการบินก็ดูแล้วน่าจะเป็นธุรกิจที่มีอนาคตดี อยู่ใน Mega trend

แต่มีปัญหาใหญ่อยู่ 3 อย่างด้วยกันสำหรับบริษัทสายการบินที่ทำให้เราไม่อาจจะลงทุนได้ง่ายๆ

1. น้ำมันเป็นต้นทุนหลักในการดำเนินธุรกิจ ผลประกอบการก็จะขึ้นอยู่กับน้ำมันพอสมควร

2. การแข่งขันสูง มีสายการบินจำนวนมากในตลาด ทำให้เกิดการแข่งราคากันอย่างรุนแรง

3. เครื่องบินมีราคาสูงมาก บริษัทต้องใช้เงินลงทุนมหาศาลในการดำเนินธุรกิจ

.

เราลองมาไล่ดูสายการบินในไทยว่ามีบริษัทอะไรบ้าง หลักๆมีอยู่ด้วยกัน 6 สายการบิน

1. Thai Airways (THAI)
2. Thai Smile
3. Nok Air (NOK)
4. Air Asia (AAV)
5. Bangkok Airways (BA)
6. Thai Lion Air

มาดู Marketshare ของแต่ละสายการบิน (2015 - 2017)

Thai Airways: 11% -> 9.1% -> 8.3%
Thai smile: 8.7% -> 10.5% -> 9.8%
Nok Air: 24.8% -> 20.3% -> 18.8%
Air Asia: 28.5% -> 29.5% -> 31.2%
Bangkok Airways: 12.3% -> 11% -> 10.8%
Thai Lion Air: 13.9% -> 18.7% -> 18.6%

.

เมื่อดูจากส่วนแบ่งการตลาดแล้วก็น่าจะตัดสินได้ไม่ยากว่าดูเหมือนผู้ชนะจะเป็น Air Asia ที่สามารถเพิ่มส่วนแบ่งได้อย่างต่อเนื่องย้อนหลังไป 3 ปี

ในขณะที่ Nok Air และ Bangkok Airways ดูแล้วกำลังงานหนัก โดยเฉพาะ Nok Air ที่มีการขาดทุนสะสมมหาศาลอีกด้วย

ลองมาดูอีกตัวเลขที่ยืนยันชัยชนะของ Air Asia กันบ้าง

ปริมาณที่นั่งต่อปี: 18.2 ล้าน -> 20.5 ล้าน -> 22.7 ล้าน
จำนวนผู้โดยสารต่อปี: 14.8 ล้าน -> 17.2 ล้าน -> 19.8 ล้าน

สรุปการแข่งขันของสายการบินภายในประเทศด้วยข้อมูลเชิงปริมาณก็จะเห็นได้ชัดว่า Air Asia มีแนวโน้มที่จะเป็นผู้ชนะค่อนข้างดีกว่าสายการบินอื่นๆ

แต่ส่วนข้อมูลเชิงปริมาณ ส่วนตัวเองไม่สามารถตอบได้ว่าสายการบินใดมีการบริการและความสะดวกสบายมากกว่ากัน เนื่องจากไม่มีโอกาสได้นั่งสายการบินอื่นมากนักนอกจากการบินไทย

.

ในเมื่อสายการบิน Air Asia ดีขนาดนี้ และยังอยู่ใน Mega trend การท่องเที่ยวที่มีอัตราการเติบโตสูง เราควรจะลงทุนใน Ais Asia เลยหรือไม่?

คำตอบนั้นไม่ง่าย

1. ถึงแม้ว่าจะมีแนวโน้มเป็นผู้ชนะในธุรกิจการบิน แต่ก็ยังต้องใช้น้ำมันเป็นต้นทุนการดำเนินงาน

2. การแข่งขันที่สูงทำให้การครอง Marketshare ที่มากขึ้น มาพร้อมกับราคาตั๋วโดยสารเฉลี่ยที่ลดลงเรื่อยๆในแต่ละปี

3. แม้ว่า Air Asia จะมีแนวโน้มดีที่สุด แต่ในเรื่องของการทำกำไรก็ยังไม่สามารถเทียบได้เลยกับสายการบินที่ Warrent Buffett ได้ซื้อไปอย่าง Southwest และ Delta

 และเมื่อเปรียบเทียบ Gross Profit ของ 2 สายการบินในเครือ Buffett ก็จะพบว่าอยู่ในอัตราที่สูงกว่า 25% ด้วยกันทั้งคู่ ในขณะที่ Air Asia นั้นยังอยู่เพียงราวๆ 15% เท่านั้น

.

ถ้าเราสนใจในธุรกิจสายการบิน Air Asia ก็อาจจะเป็นคำตอบที่มีโอกาสสร้างผลตอบแทนที่ดีให้แก่เรา

แต่หากเทียบกับธุรกิจอื่นๆอีกมากมายแล้ว Air Asia อาจจะยังขาดคุณสมบัติที่จำเป็นในการสร้างผลตอบแทนระยะยาว

เวลาจะเป็นเครื่องพิสูจน์

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
