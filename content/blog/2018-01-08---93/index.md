---
title: "แม้จะลดดอกเบี้ย แต่ KTC ก็ไม่สน"
slug: "/93"
date: "2018-01-08T08:37:00.000Z"
description: "เมื่อ 1 กันยายน 2560 ธนาคารแห่งประเทศไทยได้ประกาศจะควบคุมบัตรอัตราดอกเบี้ยของเครดิตและสินเชื่อส่วนบุคคล โดยดอกเบี้ยจะลดลงจาก 20% เหลือ 18% เท่านั้น"
image: "./images/electronic-payments-2570939_1280_copy_1_rifwr3.jpg"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","เศรษฐกิจ","หุ้นเด้ง","ktc"]
---

เมื่อ 1 กันยายน 2560 ธนาคารแห่งประเทศไทยได้ประกาศจะควบคุมบัตรอัตราดอกเบี้ยของเครดิตและสินเชื่อส่วนบุคคล โดยดอกเบี้ยจะลดลงจาก 20% เหลือ 18% เท่านั้น

รายละเอียดคือ
- คนที่เงินเดือนไม่เกิน 3 หมื่นบาทสามารถที่จะกู้ได้ไม่เกิน 1.5 เท่า 3 หมื่น
- 5 หมื่น ได้ไม่เกิน 3 เท่า จากเดิมที่กำหนดว่าให้กู้ได้ไม่เกิน 5 เท่าของรายได้

ซึ่งจากการคาดการณ์ของทางบริษัท คิดว่าการที่มีการปรับลดดอกเบี้ยลงน่าจะมีผลกระทบประมาณ 4% ของรายได้รวม และผู้บริหารก็ยังมั่นใจว่าจากมาตราการนี้จะไม่กระทบการเติบโตของบริษัท และได้เตรียมแผนการตลาดปีหน้าไว้แล้ว

- Q3 รายได้รวมเพิ่มขึ้น 13% YoY
- Q3 กำไร 846 ลบ. เพิ่มขึ้น 8% QoQ แต่ 32% YoY
- ปริมาณการใช้บัตรเครดิต +7.1% ในขณะที่ตลาด +4.2%
- ลูกหนี้ขยายตัว 10% YoY บัตรเครดิต 44,000 ลบ. สินเชื่อ 24000 ลบ.
- NPL ลดลงเหลือ 1.46% ซึ่งน้อยมาก ควบคุมดีเยี่ยม

นอกจากจะเติบโตดีเยี่ยมแล้ว บริษัทยังมีการตั้งสำรองเผื่อไว้มากถึง 565% ซึ่งถือว่าเกินความพอดีไปอย่างมาก

ใน 4Q2560 และปี 2561 จะท้าทายบริษัทอย่างมากว่าจะถูกผลกระทบหรือไม่จากการควบคุมดอกเบี้ย แต่เท่าที่ดูจาก 3Q2560 ยังไม่มีสัญญานเท่าไหร่

แต่หากมีผลกระทบจริง บริษัทก็ยังสามารถลดการตั้งสำรองลงได้ไม่มีปัญหาแต่อย่างใด และอาจจะทำให้บริษัทยังสามารถคงการเติบโตเอาไว้ได้

แต่โดยรวมแล้วถึงจะมีการควบคุมดอกเบี้ย บริษัทก็ยังมีอัตราการเติบโตของลูกค้าและการใช้บัตรที่ดีมาก การทำ promotion บัตรเครดิตของบริษัทดูเหมือนจะเด่นกว่าคู่แข่งอย่างมาก

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
