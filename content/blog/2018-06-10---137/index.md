---
title: "เจงกิสข่าน ผู้ผนึกมองโกลให้เป็นปึกแผ่น"
slug: "/137"
date: "2018-06-10T00:16:09.000Z"
description: "ตั้งแต่ช่วงปี ค.ศ. 1127 เป็นต้นมา หลังจากที่อาณาจักรจินล้มล้างเหลียวลงได้สำเร็จ จินก็เปิดฉากสงครามร้อยปีกับอาณาจักรซ่ง"
image: "./images/maxresdefault.jpg"
featured: false
draft: false
tags: ["Mongolia","Genghis Khan","ประวัติศาสตร์", "การลงทุน"]
---

ตั้งแต่ช่วงปี ค.ศ. 1127 เป็นต้นมา หลังจากที่อาณาจักรจินล้มล้างเหลียวลงได้สำเร็จ จินก็เปิดฉากสงครามร้อยปีกับอาณาจักรซ่ง ซึ่งในระหว่างนี้ มองโกลยังแบ่งเป็นหลายชนเผ่าเร่ร่อนอยู่ตามทุ่งหญ้า

แม่ทัพคนหนึ่งนามว่า “งักฮุย” ของเมืองซ่งได้โต้ตอบอาณาจักรจินกลับอย่างกล้าหาญเป็นเวลานับสิบปี ก่อนที่จะถูกใส่ความทางการเมือง ซึ่งเป็นเหตุให้ต้องโทษประหารชีวิตในเวลาต่อมา

“งักฮุย” ผู้นี้ได้รับการยกย่องจากคนในยุคนั้น เป็นเทพเจ้าแห่งความซื่อสัตย์ ซึ่งต่อได้ถูกเปลี่ยนมาเป็น “กวนอู”โดยชาวจีนยุคแมนจูในภายหลัง เนื่องจากความมั่นคงทางการเมือง

นอกจากอาณาจักรจินจะมีสงครามกับซ่งอย่างต่อเนื่องตลอดเวลาหลายทศวรรษ ชาวซีตันที่เคยถูกอาณาจักรจินล้มล้างก็ยังคอยสู้รบกันอยู่ทางตอนเหนือของอาณาจักรจินเช่นกัน

จนมาถึงในช่วงปี ค.ศ. 1145 จินและซ่งก็ได้เซ็นสนธิสัญญาสงบศึก สงครามที่ดำเนินมาหลายทศวรรษได้ยุติลง โดยที่อาณาจักรซ่งจะต้องส่งเงินทองให้ทุก ๆ ปี

แต่ถึงแม้สงครามจะยุติลงชั่วคราว ชาวซ่งและชาวจินก็ไม่ได้ลงรอยกันด้วยดี ทำให้สัญญาสงบศึกไม่อาจรั้งไว้ได้นาน จนถึงปี ค.ศ.​ 1161 จินก็เริ่มกลับมาบุกซ่งอีกครั้ง

ในขณะเดียวกันกับช่วงที่จินกำลังออกบุกโจมตีซ่ง การจลาจลภายในอาณาจักรจินก็ได้ก่อตัว และทำให้เกิดการล้มล้างผู้นำขึ้นโดยชาวจินด้วยกันเองและชาวซีตัน

นอกจากกองทัพของจินจะแพ้ซ่งในสงครามแล้ว กลุ่มล้มล้างผู้นำก็ได้สังหารผู้นำของอาณาจักรจิน และหลังจากผู้นำคนใหม่ขึ้นครอง สงครามกับซ่งก็ได้ยุติลงอีกครั้ง ซึ่งช่วงหลังจากนี้นับเป็นเวลานานที่สุดที่จินและซ่งไม่ก่อสงครามกัน

ถึงแม้จินจะเลิกรุกรานซ่งชั่วคราว และอาณาจักรของจินจะไม่ได้ขยายออกไปให้กว้างใหญ่ขึ้น แต่ช่วงนี้ก็อาจจะเป็นช่วงที่มีความสงบสุขมากที่สุดของชาวจินก็เป็นได้

เจงกิสข่านกำเนิดเมื่อปี ค.ศ. 1162 ในเผ่าหนึ่งของมองโกล แต่เดิมทรงมีพระนามว่า “เตมูจิน”  หากนึกถึงชาวมองโกล เราจะนึกถึงภาพของเหล่านักรบบนหลังม้า เพราะชาวมองโกลขี่ม้าเป็นตั้งแต่เด็กทั้งผู้หญิงและผู้ชาย

ในตอนนั้นมองโกลเป็นดินแดนรอบนอกทางตอนเหนือของจีน ซึ่งชาวจีนฮั่นในสมัยนั้นมักจะเรียกผู้คนรอบนอกว่า “พวกป่าเถื่อนนอกด่าน”

เนื่องจากอารยธรรมของคนที่อยู่ในเผ่าตามทุ่งหญ้า เร่ร่อนผลัดเปลี่ยนดินแดนไปเรื่อย อาศัยอยู่ในกระโจม ชอบสู้รบกันเองระหว่างเผ่าเพื่อแย่งชิงดินแดน ปล้นสะดม หรือแม้แต่การล้างแค้นไปมา

แต่หนึ่งในลักษณะนิสัยที่สำคัญที่ทำให้เตมูจินสามารถผนึกมองโกลได้ คือการให้อภัยแก่คนที่เคยทรยศ ซึ่งเป็นนิสัยที่หาได้ยากมาก ทำให้เตมูจินสามารถเอาชนะใจผู้คนได้อย่างมากมาย

นอกจากนี้ เมื่อเตมูจินสามารถตีชนเผ่าอื่น ๆ ได้สำเร็จ เตมูจินจะไม่ล้มล้างทั้งชนเผ่า แต่จะเสนอให้แต่ละชนเผ่าเข้ามาอยู่ในการคุ้มครองของเผ่าตน นวัตกรรมทางการเมืองนี้เป็นอีกปัจจัยสำคัญที่ทำให้ผู้คนซื่อสัตย์ต่อเตมูจิน

ซึ่งทั้งหมดก็ขึ้นอยู่กับว่า หากเตมูจินต้องการเพียงรุกรานเผ่าอื่นไปวัน ๆ ก็คงไม่จำเป็นต้องให้อภัยแก่คนที่ทรยศเท่าไหร่นัก หรือก็คงไม่จำเป็นที่จะต้องไว้ชีวิตคนในเผ่าและรอวันที่จะโดนกลับมาล้างแค้น

แต่เป้าหมายของเตมูจินนั้นยิ่งใหญ่ เตมูจินยอมสละผลประโยชน์ส่วนตนเพื่อการที่ใหญ่กว่า นั่นก็คือการผนึกมองโกลให้เป็นปึกแผ่น

ก็คงเฉกเช่นเดียวกับตัวเรา หากเราไม่ได้มีเป้าหมายที่ใหญ่มากนัก ผลประโยชน์ส่วนตนคงจะบังตาและไม่สามารถก้าวข้ามไปสู่สิ่งที่ยิ่งใหญ่กว่าตนเองได้

เตมูจินได้เริ่มมีอิทธิพลในช่วงปี ค.ศ. 1186 หลังจากที่ได้รับเลือกเป็นข่านแห่งมองโกล

การรวบรวมมองโกลในช่วงแรกเต็มไปด้วยความยากลำบาก เตมูจินต้องเจอกับการต่อต้านจากชนเผ่าอื่น ๆ รวมถึงการผิดใจกับสหายหลายครั้งเพราะเกรงกลัวอิทธิพลที่เริ่มยิ่งใหญ่ขึ้น

แต่ด้วยความสามารถของเตมูจินประกอบกับความจงรักภักดีของคนในเผ่า ทำให้เตมูจินค่อย ๆ ควบรวมเผ่าอื่น ๆ ได้เพิ่มอย่างต่อเนื่อง

ช่วงปี ค.ศ. 1200 เผ่ามองโกลของเตมูจินได้เริ่มกลายเป็นเผ่าใหญ่ และเหลือเพียงชาวมองโกลอื่น ๆ อีกไม่กี่เผ่าที่เตมูจินจะต้องรวบรวมเพื่อทำให้มองโกลทั้งหมดรวมกันเป็นปึกแผ่น

ในช่วงนี้เองที่มองโกลเริ่มมีอำนาจมากขึ้น อาณาจักรจินก็ได้รับอิทธิพลจากมองโกลที่แข็งกร้าวขึ้นเช่นกัน ทำให้เกิดความตึงเครียดระหว่างสองอาณาจักร

นอกจากอาณาจักรจินจะต้องคอยระวังชาวมองโกลทางเหนือแล้ว ยังต้องคอยระวังอาณาจักรซ่งจากทางใต้ที่มีปัญหากันมานานนับศตวรรษ

หากดูจุดยุทธศาสตร์ของอาณาจักรจินแล้วถือว่าเสียเปรียบอย่างมาก เพราะถูกล้อมรอบด้วยมองโกลและซ่ง ซึ่งครั้งก่อนจินเองก็เคยรุกรานอาณาจักรเหล่านี้อยู่เป็นประจำ

ในปี ค.ศ. 1204 เตมูจินสามารถรวบรวมเผ่ามองโกลทั้งหมดได้สำเร็จ นับเป็นครั้งแรกที่ชาวมองโกลมีข่านสูงสุดเพียงคนเดียว

แต่ทว่าเตมูจินไม่ได้หยุดเพียงแค่นี้ และยังได้เริ่มรุกรานอาณาจักรซีเซี่ยและอาณาจักรจินอย่างต่อเนื่อง

ปี ค.ศ. 1206 เตมูจินได้ขึ้นเป็นผู้นำสูงสุดของมองโกลและได้รับสมญานามว่า “เจงกิสข่าน” ซึ่ง เจงกีส แปลว่า “เวิ้งสมุทร หรือ มหาสมุทร” จึงสามารถเปรียบได้ว่า ข่าน ที่มีความยิ่งใหญ่ ดั่งมหาสมุทร

เราอาจจะสังเกตุได้ว่าความแข็งแกร่งมองโกลน่าจะเริ่มมาจากที่หลาย ๆ เผ่ารวมใจกันเป็นหนึ่ง และต่อสู้เพื่อสิ่งที่ยิ่งใหญ่กว่าตนเอง

ชาวมองโกลพื้นฐานเป็นนับรบอยู่แล้ว การขี่ม้ายิ่งธนูถือว่าไม่มีชนชาติใดเทียบได้ติด หากร่วมแรงใจกันเป็นหนึ่งเดียว การทำการใหญ่ก็อาจจะไม่ใช่เรื่องยากอีกต่อไป

ใกล้เคียงกับคำที่ว่า “หากต้องการจะไปเร็ว ให้ไปคนเดียว แต่หากต้องการจะไปได้ไกล ให้ไปเป็นทีม”

---

Credit: http://guawesome.com/blog/2018/03/25/เจงกิสข่าน-ผู้ผนึกมองโก/
