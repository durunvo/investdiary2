---
title: "กระจายความเสี่ยงมากหรือน้อยดี?"
slug: "/61"
date: "2017-10-11T08:53:32.000Z"
description: "่หลายครั้งที่เราจะได้ยินคำว่าการลงทุนควรกระจายความเสี่ยงให้เหมาะสม ในแง่ของเชิงทฤษฎีก็ค่อนข้างสมเหตุสมผล"
image: "./images/parachute-1209920_1920_ie8zku.jpg"
featured: false
draft: false
tags: ["ผู้บริหาร","กระจายความเสี่ยง","การลงทุน"]
---

หลายครั้งที่เราจะได้ยินคำว่าการลงทุนควรกระจายความเสี่ยงให้เหมาะสม ในแง่ของเชิงทฤษฎีก็ค่อนข้างสมเหตุสมผล มันคงเป็นเรื่องไม่ดีแน่ๆหากบริษัทที่เราตัดสินใจลงทุนเกิดเหตุการณ์ไม่คาดฝันขึ้นซึ่งทำให้เงินทั้งหมดของเราหายไป แต่ในทางปฏิบัติมันก็บอกได้ยากว่ากฏเกณฑ์ที่เหมาะสมควรจะเป็นอะไร

การกระจายความเสี่ยงที่เราได้ยินกันบ่อยครั้งนั้น ส่วนใหญ่มาจากมุมมอง 2 มุมมองหลักๆด้วยกัน

1. มุมมองของนักวางแผนทางการเงิน ผู้แนะนำการลงทุน หรือเรียกสั้นๆว่า "มืออาชีพ"
2. มุมมองจากนักลงทุน

ทั้ง 2 มุมมองล้วนให้ความสำคัญกับการกระจายความเสี่ยงอย่างมาก แต่ในทางปฏิบัติแล้วกลับมีวิธีการที่แตกต่างกันไปอย่างสิ้นเชิง แต่อย่างไรก็ตามก็ยังถือว่าเป็นหลักการที่ควรจะให้ความสำคัญทั้งคู่ เพราะแต่ละแง่ก็มีจุดประสงค์ที่ดีต่อเจ้าของเงินทุน

ในมุมมองของ "มืออาชีพ" การแนะนำให้กระจายความเสี่ยงนั้นส่วนใหญ่จะมีกลุ่มเป้าหมายคือบุคคลทั่วไปที่ไม่ใช่นักลงทุนมืออาชีพ บุคคลทั่วไปที่กล่าวถึงก็คือผู้ที่ไม่มีเวลาศึกษาการลงทุนมากนัก แนวทางการกระจายความเสี่ยงก็จะเน้นการกระจายไปยัง asset class ต่างๆ เช่น กองทุนรวม กองทุนตลาดเงิน กองทุนหุ้นในประเทศ กองทุนหุ้นต่างประเทศ เหตุผลที่จำเป็นต้องกระจายการลงทุนไปมากก็เพราะผู้ลงทุนไม่มีเวลาศึกษามากนัก การกระจายความเสี่ยงจึงต้องมากขึ้นตามระดับความรู้ความสามารถ เพราะหากมีบางอย่างผิดพลาดก็จะไม่ส่งผลมากต่อความมั่งคั่งโดยรวม

ส่วนการกระจายความเสี่ยงในมุมของนักลงทุนก็จะให้ในมุมมองสำหรับบุคคลที่เป็นนักลงทุนอยู่แล้ว คำแนะนำก็คือควรกระจายความเสี่ยงไม่ให้มากเกินไป เพราะหากเป็นแบบนั้นผลตอบแทนก็จะเข้าใกล้ค่าเฉลี่ยมากขึ้น ซึ่งท้ายที่สุดแล้วจะไม่ต่างอะไรกับการลงทุนในกองทุน แต่ในขณะเดียวกันก็ไม่ควรนำเงินทั้งหมดซื้อทรัพย์เพียงไม่กี่ตัว เพราะหากเกิดเหตุการณ์ไม่คาดฝันก็จะมีผลต่อความมั่งคั่งโดยรวมอย่างมีนัย จำนวนสินทรัพย์ที่เราควรจะมีอย่างต่ำคือ 5 ตัว

เรามาดูการกระจายความเสี่ยงของบุคคลระดับโลกกันบ้าง ไม่ว่าจะเป็นการลงทุนในการสร้างบริษัทหรือลงทุนในส่วนของตลาดเงิน ผมสังเกตุหลายคนจริงๆแล้วต่างก็รวยขึ้นจากการลงทุนในไม่กี่อย่าง เช่น Bill Gates ก็ร่ำรวยขึ้นมาจากการสร้าง Microsoft. Steve Jobs ก็ร่ำรวยมากจาก Apple. Mark Zuckerburg ก็ร่ำรวยขึ้นมาจาก Facebook. และอีกหลายๆคนที่ไม่ได้กล่าวถึงก็ล้วนแต่เป็นลักษณะเช่นเดียวกัน มีเพียง Warren Buffett เท่านั้นที่ร่ำรวยมาจากการลงทุนในทรัพย์สินมากกว่า 1 อย่าง

พอจะสรุปได้ว่าการกระจายความเสี่ยงนั้นขึ้นอยู่กับเป้าหมายของเรา สำหรับคนที่รับเงินเดือนและหวังจะมีเงินใช้พอเพียงหลังเกษียณผมคิดว่าเหมาะมากกับการกระจายความเสี่ยงตามคำแนะนำของ "มืออาชีพ" แต่หากหวังการเปลี่ยนแปลงชีวิตครั้งใหญ่ก็อาจจะเป็นเรื่องยาก ส่วนต่อมาคือผู้ที่คาดหวังจะเปลี่ยนแปลงชีวิตแต่ไม่ต้องการความเสี่ยงมากเกินไปก็น่าจะเหมาะกับคำแนะนำจากนักลงทุน แต่พึงระวังไว้เสมอว่าการเปลี่ยนแปลงชีวิตโดยการลงทุนจะต้องใช้เวลา สำหรับคนที่ไม่เหมาะกับการลงทุนก็อาจจะเหมาะกับการสร้างธุรกิจขึ้นมาเอง

ท้ายที่สุดแล้วทั้ง 3 วิธีการต่างมีความเสี่ยงในระดับที่แตกต่างกัน ยิ่งกระจายความเสี่ยงน้อยก็จะต้องยิ่งมีความรู้มากขึ้นในทรัพย์สินที่ลงทุน โดยเฉพาะหากเรามีทรัพย์สินเพียงแค่อย่างเดียวด้วยการสร้างธุรกิจ ความรู้ความสามารถของเราจะต้องอยู่ในระดับที่สูง ผมจึงขอสรุปง่ายๆเหมือนที่ Warren Buffett ได้กล่าวไว้ว่า "การกระจายความเสี่ยงจะจำเป็นต่อเมื่อเราไม่รู้ว่าเราทำอะไร"

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
