---
title: "จุดเริ่มต้นอาณาจักรมองโกล"
slug: "/136"
date: "2018-06-09T17:15:01.000Z"
description: "ในยุคที่รุ่งเรืองที่สุดของอาณาจักรมองโกลสมัยกุบไลข่าน (หลานของเจงกิสข่าน)"
image: "./images/image.jpg"
featured: false
draft: false
tags: ["การลงทุน","Mongolia","Genghis Khan","ประวัติศาสตร์"]
---

ในยุคที่รุ่งเรืองที่สุดของอาณาจักรมองโกลสมัยกุบไลข่าน (หลานของเจงกิสข่าน)

อาณาจักรมองโกลครอบคลุมตั้งแต่ตะวันออกของทวีปเอเชียซึ่งปัจจุบันคือประเทศจีน ไปจนถึงเอเชียตะวันออกกลาง และได้ขยายไปจนถึงยุโรป

แต่ก่อนที่มองโกลจะยิ่งใหญ่และรุ่งเรืองถึงขีดสุด ก่อนหน้านั้นมองโกลเป็นเพียงแค่ชนเผ่าที่กระจัดกระจายตามเชิงเขาทางตอนเหนือของประเทศจีน

อาณาจักรมองโกลเกิดขึ้นมาได้อย่างไร? เหตุใดมองโกลที่เคยเป็นเพียงชนเผ่าที่กระจัดกระจาย ถึงสามารถล้มล้างและครอบครองอาณาจักรของจีนในยุคนั้นได้?

แม้อาณาจักรของมองโกลจะยิ่งใหญ่มากที่สุดในประวัติศาสตร์ของโลก แต่ยังมีอาณาจักรเล็ก ๆ หลายอาณาจักรที่ยังสามารถอยู่รอดจากมองโกลได้อย่างน่าแปลกใจ โดยเฉพาะ “ญี่ปุ่น”

ประเทศญี่ปุ่นถือว่าอยู่ใกล้ดินแดนมองโกลในสมัยนั้นมาก เมื่อเปรียบเทียบกับฝั่งยุโรป

แต่ประเทศเล็ก ๆ บนเกาะแห่งนี้ เทคโนโลยีและการทหารไม่สามารถเทียบมองโกลติดเลยแม้แต่น้อย กลับสามารถรอดพ้นการรุกรานจากชาวมองโกลได้อย่างไร ?

เราลองมาย้อนประวัติของชาวมองโกลในสมัยนั้นกันดูก่อน

ในปี ค.ศ. 907 ยุคต่อจากราชวงค์ถัง อาณาจักรฮั่นของจีนได้แตกแยกออกเป็นหลายอาณาจักร ซึ่งในยุคนั้นเรียกว่า “ยุคห้าราชวงค์สิบอาณาจักร”

ชาวซีตันซึ่งเป็นชนเผ่าหนึ่งในมองโกล ได้ก่อตั้งอาณาจักรเหลียวขึ้นหลังจากที่ราชวงค์ถังสิ้นสุดลง ส่วนแผ่นดินจีนฮั่นก็แตกแยกออกเป็นหลายอาณาจักรและหลายราชวงค์

ต่อมาปี ค.ศ. 960 ราชวงค์ซ่งได้ถูกสถาปนาขึ้นเพื่อรวบรวมแผ่นดินจีนฮั่นให้เป็นปึกแผ่นอีกครั้ง และหลังจากนั้นในปี ค.ศ. 1038 อาณาจักรซีเซี่ยก็ได้ถูกสถาปนาขึ้นในภาคตะวันตกติดกับทิเบต

ในยุคของราชวงค์ซ่ง ประชากรของอาณาจักรซ่งมีการเติบโตเป็นเท่าตัวภายใน 1 ศตวรรษ ซึ่งการเติบโตที่มหาศาลเช่นนี้ ทำให้เป็นยุคที่รุ่งเรืองทั้งการค้า ศิลปกรรม วัฒนธรรม เกษตรกรรม

แต่การทหารของในยุคซ่งกลับอ่อนแอ เพราะมีการลดทอนอำนาจของทหารตั้งแต่ต้นราชวงค์ เพื่อลดโอกาสการยึดอำนาจแลกกับความมั่นคงของบ้านเมือง

ทำให้ในระหว่างนี้ ซ่งจึงถูกอาณาจักรเหลียว และซีเซี่ยรุกรานอยู่เป็นประจำ เนื่องจากอาณาจักรซ่งมีความอุดมสมบูรณ์มากกว่า

เราอาจจะได้เรียนรู้ว่า ถึงแม้อาณาจักรซ่งจะมีความเจริญรุ่งเรืองและมั่งคั่งอย่างมากในยุคนั้น แต่การขาดความสามารถในการป้องกันอาณาจักร เป็นเหตุให้ถูกคุกคามอยู่เป็นประจำ

แต่ซ่งก็ได้อาศัยจุดแข็งเรื่องความมั่งคั่งยับยั่งการรุกรานได้อยู่เรื่อยมา โดยการยอมส่งเงินทองให้อาณาจักรรอบ ๆ อยู่เป็นประจำ เพื่อแลกกับความสงบของบ้านเมือง

ความแตกต่างระหว่างชนเผ่าทางเหนือและชาวฮั่นหลัก ๆ คือเรื่องของธรรมเนียมและวัฒนธรรม

ชนเผ่าทางเหนือจะเร่รอน และย้ายที่อยู่ไปเรื่อย อาศัยอยู่ในกระโจมเป็นหลัก ผู้คนในแถบเหนือจึงมีวัฒนธรรมที่ไม่ค่อยพัฒนาเหมือนซ่ง

แต่การเร่ร่อนก็ทำให้ผู้คนทางเหนือขี่ม้าเก่ง มีความเป็นนักรบในสายเลือด เพราะต้องเอาตัวรอดในภูมิประเทศที่ไม่อุดมสมบูรณ์ ต่างจากชาวฮั่นที่อาศัยในเมือง ชีวิตสงบสุข และเน้นค้าขายเป็นหลัก

ชนเผ่าหนี่ย์เจินหรือบรรพบุรุษของชาวแมนจู อาศัยอยู่ทางตอนเหนือของจีน (ใกล้เคียงกับเกาหลีเหนือในปัจจุบัน) ในขณะนั้นยังเป็นชนเผ่าเล็ก ๆ และมีปัญหากับอาณาจักรเหลียวมาช้านาน

ต่อมาในปี ค.ศ. 1114 ชนเผ่าหนี่ย์เจินได้เริ่มรวบรวมชนเผ่าขึ้น และโจมตีอาณาจักรเหลียวอย่างต่อเนื่อง จากนั้นในปีต่อมาก็ได้สถาปนาอาณาจักรจิน

ปี ค.ศ. 1121 อาณาจักรจินได้ทำสนธิสัญญากับซ่งเพื่อล้มล้างอาณาจักรเหลียว จนท้ายที่สุด 4 ปีต่อมาก็สามารถทำให้สิ้นสุดอาณาจักรเหลียวได้สำเร็จ

เมื่ออาณาจักรเหลียวแตก ทำให้ชาวฮั่นที่อาศัยภายใต้การปกครองของเหลียว รวมถึงชนเผ่ามองโกลบางส่วน ก็ถูกกลืนเข้าสู่อาณาจักรของจิน

แต่หลังจากอาณาจักรจินได้ล้มล้างเหลียวเรียบร้อยแล้ว ก็กลับทำลายสนธิสัญญากับซ่งและเริ่มบุกรุกซ่งทางตอนเหนือ

เรียกได้ว่าเมื่อคนเราเริ่มมีอำนาจ ความหลงใหลในอำนาจก็ยากที่จะหยุดลงได้ ซึ่งประวัติศาสตร์ได้สอนเราเรื่องนี้อยู่บ่อยครั้ง และมันก็จะเป็นเช่นนี้ต่อไปในอนาคต

ในปี ค.ศ.1127 อาณาจักรจินสามารถยึดเมืองหลวงไคฟงของซ่งได้สำเร็จ และทำให้อาณาจักรซ่งสูญเสียดินแดนทางตอนเหนือ

ชาวซ่งจึงต้องย้ายเมืองหลวงลงมาอยู่ทางตอนใต้ ซึ่งในยุคนี้จะเรียกว่าอาณาจักรซ่งใต้ และศึกระหว่างจินและซ่งก็ดำเนินตั้งแต่นั้นเป็นต้นมา

ในอดีตคำว่าอาณาจักรอาจจะเหมือนถึงดินแดนและเขตปกครอง เราอาจจะสู้รบเพื่อครอบครองอาณาจักร เพราะยิ่งมีอาณาจักรที่กว้างใหญ่ ยิ่งหมายถึงอำนาจและเงินทองที่มากขึ้น

แต่ปัจจุบันคำว่าอาณาจักรอาจจะสามารถเปรียบได้กับธุรกิจ เราสู้รบกันทางปัญญาและครอบครองอาณาจักรทางธุรกิจ เพราะยิ่งธุรกิจใหญ่ขึ้น หมายถึงอำนาจและเงินทองที่มากขึ้นเช่นกัน

ดังนั้นไม่ว่ายุคสมัยใด การสู้รบกันเพื่อแก่งแย่งอาณาจักรและทรัพยากรก็เกิดขึ้นอยู่ตลอดเวลา หากเรานำข้อคิดที่เคยเกิดขึ้นในอดีตมาศึกษา เราจะพบว่าแท้จริงแล้วหลักการไม่ได้มีอะไรเปลี่ยนแปลงเลย

ในระหว่างที่จินและซ่งทำสงครามกัน จุดกำเนิดของอาณาจักรมองโกลก็ใกล้จะเริ่มขึ้น ชนเผ่ามองโกลที่เร่ร่อนหลายเผ่าใกล้จะเริ่มต้นผนึกรวมกันเป็นปึกแผ่น โดยชายผู้มีนามว่า “เตมูจิน”

---

Credit: http://guawesome.com/blog/2018/03/16/จุดเริ่มต้นอาณาจักรมอง/
