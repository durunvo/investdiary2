---
title: "SSP - หุ้น Solar น้องใหม่"
slug: "/106"
date: "2018-01-11T13:46:55.000Z"
description: "บริษัท เสริมสร้าง พาวเว่อร์ จำกัด (มหาชน) SSP เริ่มบริษัทเมื่อปี 2010 เป้าหมายของบริษัทคือการมีกำลังผลิตทั้งหมด 200 MW ภายในปี 2563"
image: "./images/solar-energy-2157212_1280_copy_1_lwupx5.jpg"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","Solarfarm","SSP"]
---

บริษัท เสริมสร้าง พาวเว่อร์ จำกัด (มหาชน) SSP เริ่มบริษัทเมื่อปี 2010

เป้าหมายของบริษัทคือการมีกำลังผลิตทั้งหมด 200 MW ภายในปี 2563 และปัจจุบันมีอยู่ในมือแล้วทั้งหมด 166.8 MW

ถึงแม้บริษัทจะยังใหม่ แต่ทีมผู้บริหารเองก็มี 1 โครงการที่ดำเนินการแล้วในประเทศไทย และตัวผู้บริหารเองก็มาจากบริษัทโรงไฟฟ้าแห่งรายใหญ่ในประเทศ

โรงไฟฟ้าแห่งแรกสัญญาซื้อขายกับ EGAT เริ่ม COD ปี 2558 โดยมีกำลังผลิต 40 MW และได้ Adder 6.5 บาท

ส่วนอีกโครงการในไทย Solar WVO บริษัทได้เซ็นสัญญาแล้วจำนวนอีก 5 MW คาดว่า COD 4Q2561 ส่วนนี้บริษัทได้ FiT ประมาณ 4.12 บาท ถือว่าพอใช้ได้

ต่อมาบริษัทได้เข้ามาสู่ Solar ในญี่ปุ่น

1) Hidaka 18.1 MW ได้ FiT 40 เยน คาดว่าจะ COD 1Q2561 
2) Yamaka 30 MW ได้ FiT 36 เยน เริ่มก่อสร้าง 3Q2560 คาดว่า COD 2Q2563
3) Zouen 6 MW ได้ FiT 36 เยน เริ่มก่อสร้าง 4Q2560 คาดว่า COD 4Q2561
4) Leo 40 MW ได้ FiT 36 เยน ยังไม่เริ่มก่อสร้าง คาดว่า COD 2Q2563
5) Yamaka2 10 MW ได้ FiT 36 เยน ยังไม่เริ่มก่อสร้าง คาดว่า COD 2Q2563

โดยรวมแปลว่าปลายปี 2018 บริษัทจะมีกำลังผลิตเพิ่มจาก 40 MW เป็น ประมาณ 70 MW คิดเป็นการเติบโต 80% ที่น่าสนใจคือบริษัทได้ค่าไฟในประเทศญี่ปุ่นค่อนข้างดีพอสมควร IRR น่าจะสูงระดับหนึ่ง

ส่วนที่วุ่นวายที่สุดสำหรับบริษัท SSP คือในส่วนของงบการเงิน โดยเฉพาะการลงทุนในญี่ปุ่นจะต้องลงทุนผ่านบริษัทใน Hong Kong และก่อให้เกิดผลกำไรขาดทุนจากอัตราแลกเปลี่ยนด้วย

มีตัวเลข 3 ตัวที่ทำให้ผลประกอบการของบริษัทไม่สะท้อนความเป็นจริง

1. Unrealize FX gain/loss ซึ่ง 9M2560 ขาดทุน 48 ลบ. ปีที่แล้วกำไร 29 ลบ.

 การที่บริษัทต้องลงทุนในญี่ปุ่นผ่านบริษัท Holding ที่ Hong Kong เนื่องจากผลประโยชน์ทางภาษีที่ดีกว่า บริษัทใช้วิธีปล่อยกู้ให้บริษัทใน Hong Kong เนื่องจากสามารถรับเงินกลับได้ง่ายกว่าการใส่เงินทุน แต่ในทางบัญชีแล้วเวลาปิดงบ Auditor จะต้องบันทึกผลกำไรขาดทุนจากอัตราแลกเปลี่ยน

 แต่ความจริงแล้ว Gain หรือ loss จากอัตราแลกเปลี่ยนส่วนนี้ไม่มีผลกระทบต่อกระแสเงินสดทั้งสิ้น

2. ต้นทุนการลงทุนบางส่วน Auditor ไม่ยอมให้บันทึกเป็นสินทรัพย์แต่ให้นำมาตัดเป็นค่าใช้จ่าย สำหรับบริษัทแล้วเงินจำนวนนี้ได้รวมอยู่ในงบลงทุน ดังนั้นการบันทึกค่าใช้จ่ายส่วนนี้จะไม่ได้กระทบกระแสเงินสด

3. ภาษีที่ไม่ได้เกิดขึ้นจริง

ดังนั้นหากเรารายการเหล่านี้ออกจากงบ จะทำให้ได้ผลประกอบการที่แม่นยำมากขึ้น บริษัทเรียกว่า Adjusted Operaing income ซึ่งบริษัทจะใช้ตัวนี้เป็นหลักในการประเมิณผลการดำเนินงานและพิจารณาจ่ายปันผล

มาดูผลประกอบการ

- กำไรขั้นต้นดีขึ้น 1.4% YoY มาจากปริมาณขายที่เยอะขึ้น ถึงแม้ว่าจะได้ราคาขายต่ำลง
- Adjusted operating profit ลดลง - 2.2% แต่ถ้าไปดูงบแบบไม่ปรับก็จะทำให้กำไรลดลงเยอะกว่านี้มาก

สรุป

- เป็นหุ้นที่ยังไม่ได้รับความสนใจมากนัก แต่บริษัทก็มีแผนจะเติบโตอีก 4 เท่าตัวภายในปี 2563 ในแง่ของจำนวน MW
- การก่อสร้างอาจจะมีความล่าช้าเกิดขึ้นได้ อยากให้ใส่ส่วนเผื่อความปลอดภัยเข้าไปก่อนคำนวนสักหน่อย ถึงแม้ว่าบริษัทจะประเมิณเผื่อไว้บ้างแล้วก็ตาม
- บริษัทยังเล็กอยู่ อาจจะต้องเสียดอกเบี้ยเงินกู้ในอัตราที่สูงกว่ารายใหญ่ แต่ดูจากค่าไฟที่บริษัทได้ในประเทศญี่ปุ่นก็ถือว่าอยู่ในเกณฑ์ที่ดี น่าจะมี IRR ราวๆ 12 - 15%
- ปัจจุบันมี D/E อยู่ 1.3 เท่า ซึ่งอยู่ในระดับที่ไม่ได้ต่ำไม่ได้สูง หากอนาคตบริษัทต้องการจะลงทุนเพิ่มก็น่าจะสามารถทำได้บ้าง บริษัทสามารถกู้ Bank ที่ญี่ปุ่นได้ โดย Term คือเงินทุน 20% และเงินกู้อีก 80%

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)

