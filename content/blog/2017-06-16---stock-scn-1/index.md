---
title: "สรุปหุ้นรายตัว - SCN"
slug: "/stock-scn-1"
date: "2017-06-16T08:04:29.000Z"
description: "บริษัท สแกน อินเตอร์ จำกัด SCN ประกอบธุรกิจด้านพลังงานเกี่ยวกับก๊าซธรรมชาติ ครอบคลุมหลายส่วนของก๊าซธรรมชาติ"
image: "./images/18814004_986254948176419_1692442393844842392_n_u9s8wv.png"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","scn"]
---

##ข้อมูลทั่วไป

บริษัท สแกน อินเตอร์ จำกัด SCN
![](./images/18740283_986254551509792_8579810622580830871_n_aznycw.png)
ประกอบธุรกิจด้านพลังงานเกี่ยวกับก๊าซธรรมชาติ ครอบคลุมหลายส่วนของก๊าซธรรมชาติ

1. สถานีบริการก๊าซธรรมชาติสำหรับรถยนต์และอุตสาหกรรม
2. ปรับปรุงคุณภาพก๊าซธรรมชาติ
3. ธุรกิจขนส่งก๊าซธรรมชาติ
4. ธุรกิจออกแบบ ผลิต รับเหมา ซ่อมบำรุงอุปกรณ์ที่เกี่ยวกับก๊าซธรรมชาติ
5. ธุรกิจติดตั้งระบบก๊าซภายในรถยนต์
6. ธุรกิจจัดจำหน่ายก๊าซธรรมชาติจากแหล่ง
7. ธุรกิจพลังงานหมุนเวียน เช่น โซล่า 5MW
8. โชว์รูมและศูนย์บริการรถยนต์ มิตซูบิชิ จำนวน 2 สาขา ในปริมณฑล
อื่นๆ

ถึงแม้บริษัทจะมีหลากหลายธุรกิจ แต่พื้นฐานแล้วจะเกี่ยวกับก๊าซธรรมชาติเป็นส่วนใหญ่ เดิมแล้วเป็นผู้รับเหมา แต่ขยายตัวเองเข้าสู่การให้บริการเพื่อเพิ่มความมั่นคงทางธุรกิจ โดยลูกค้าของบริษัทได้แก่ ปตท. และเอกชนรายอื่นๆ

![](./images/18700208_986715658130348_8881330029226452945_n_bkiy51.png)

สำหรับธุรกิจสถานีบริการก๊าซธรรมชาติ ปัจจุบันบริษัทมีสถานีบริการก๊าซธรรมชาติอยู่ 7 สถานีทั้งแบรนด์ตัวเองและภายได้แบรนด์อื่น

![](./images/18922145_986718718130042_4821676452198204126_n_yzszy1.png)

ปัจจุบันบริษัทมีกำลังผลิตจาก 7 สถานีรวม 290,000 กก./วัน โดยหากอีก 3 สถานีก่อสร้างเสร็จ จะทำให้เพิ่มขึ้นอีกถึง 160,000 กก./วัน แต่บริษัทก็มีเป้าหมายในการขยายธุรกิจส่วนนี้โดยการเข้าซื้อสถานีทำดำเนินการอยู่แล้ว เพื่อรองรับการเติบโตการใช้งาน NGV ในอนาคต โดยลูกค้าหลักคือลูกค้ารายย่อยทั่วไปที่ใช้ก๊าซธรรมชาติเป็นเชื้อเพลิง

ในส่วนของก๊าซธรรมชาติสำหรับอุตสาหกรรม มีการให้บริการขนส่งก๊าซทางรถจากแนวท่อสู่โรงงาน ลูกค้าหลักคือลูกค้าที่ต้องการใช้ก๊าซแต่ยังไม่มีแนวท่อไปถึง
บริษัทยังมีสถานีบริการก๊าซที่ตั้งอยู่ตามแนวท่อก๊าซเพื่อขนส่งไปยังสถานีนอกแนวท่อก๊าซโดยมีลูกค้าคือ ปตท. ปัจจุบันมีสถานีหลักอยู่จังหวัดปทุมธานี ที่มีกำลังการผลิตถึง 643,000 กก./วัน สามารถรองรับรถบรรทุกพร้อมกันได้ 30 คัน ตลอด 24 ชั่วโมง

ธุรกิจเกี่ยวกับรับเหมาถือมีส่วนสำคัญต่อรายได้ค่อนข้างมาก โดยบริษัทถือว่ามีบทบาทสำคัญในระดับประเทศในการก่อสร้างและติดตั้งระบบให้แก่สถานี NGV เป็นจำนวน 250 จาก 503 แห่งทั่วประเทศ และรวมถึงสถานีหลักของ ปตท. 12 จาก 17 แห่งทั่วประเทศ

สัดส่วนรายได้ของบริษัทเกิดจากธุรกิจที่เกี่ยวข้องกับก๊าซธรรมชาติ 68.5% และจำหน่ายรถยนต์ 24.69% ซึ่งในส่วนของก๊าซธรรมชาตินั้นกระจายความเสี่ยงไปยังหลายบริการ แต่การจำหน่ายรถยนต์นั้นยังถือว่ามีผลต่อรายได้ค่อนข้างมาก แต่อนาคตธุรกิจก๊าซธรรมชาติจะเติบโตและเป็นสัดส่วนรายได้ที่มากขึ้น

![](./images/18814183_986727671462480_8934843882161824586_n_gpplh0.png)

ก๊าซ NGV ถือว่าเป็นเชื้อเพลิงที่ปลอดภัย มีน้ำหนักเบากว่าอากาศจึงไม่เกิดการสะสม และมีอุณหภมิที่สามารถติดด้วยตัวเองสูงกว่าเชื้อเพลิงชนิดอื่น ปัจจุบันจึงเป็นที่สนับสนุนของภาครัฐ มีการลอยตัวราคาก๊าซเพื่อสะท้อนต้นทุนที่แท้จริงซึ่งจะสงผลดีระยะยาว จากเดิมที่ตรึงราคาไว้และกำหนดราคาค่าปลีกไม่เท่ากันสำหรับรถแต่ละประเภท

ปัจจุบันต้นทุนของการจำหน่ายก๊าซในสถานีบริการประกอบด้วย เนื้อก๊าซ ค่าผ่านท่อส่ง ค่าบริการจัดหา ค่าดำเนินการ(ค่าการตลาด) และ VAT

![](./images/18814015_986734568128457_8939987403430098420_n_gwrfcn.png)

จากการลอยตัวราคาก๊าซธรรมชาติส่งผลให้ราคาค้าปลีกที่สถานีบริการมีการปรับลงเนื่องจากต้นทุน NGV ที่ลดลง

![](./images/18920233_986736251461622_3585111111365177492_n_v0tljj.png)

แต่เนื่องจากราคาน้ำมันที่ลดลงในช่วงปีที่ผ่านมาประกอบกับมีการลอยตัวราคา NGV ทำให้จำนวนรถยนต์ใช้ NGV ลดลงและหันกลับไปใช้น้ำมันมากขึ้น แต่โดยธรรมชาติแล้ว NGV ยังคงเป็นเชื้อเพลิงที่มีราคาถูกกว่าน้ำมันถึง 50% ดังนั้นหากไม่มีเชื้อเพลิงใดที่ถูกกว่า NGV ก็จะยังคงเป็นที่ต้องการและเมื่อผู้คนตระหนักเช่นเดียวกันก็จะหันกลับมาใช้

จำนวนสถานีบริการยังมีจำนวนจำกัดเพราะจะต้องตั้งอยู่ตามแนวท่อเท่านั้น จึงมีข้อจำกัดในการขึ้นมาเป็นพลังงานหลัก

**จุดเด่น**

- บริษัทเป็นผู้นำด้านก๊าซธรรมชาติในประเทศ มีประสบการณ์มายาวนานกว่า 20 ปี มีการบริการที่ครบวงจรทุกภาคส่วนเกี่ยวกับก๊าซธรรมชาติ มีการวิจัยระบบต่างๆที่ใช้สำหรับก๊าซธรรมชาติเพื่อความปลอดภัยและประสิทธิภาพ
- ก๊าซธรรมชาติแม้ว่าจะมีแนวโน้มชะลอตัวลง แต่ก็ยังมีราคาต่ำกว่าน้ำมันถึง 50% ทำให้มีแนวโน้มที่จะเติบโตขึ้นจากการที่ผู้คนหันมาใช้ก๊าซมากขึ้น
- บริษัทหันมาเป็นผู้ให้บริการด้วยตัวเองทำให้รายได้มีความมั่นคงมากขึ้น
ยิ่งบริษัทขยาย จะยิ่งมี economy of scale

**ความเสี่ยง**

- บริษัทมีคู่ค้ารายใหญ่คือ ปตท. ดังนั้นจึงมีความเสี่ยงหากมีการยกเลิกการจ้าง แต่มีโอกาสที่น้อยเนื่องจากบริษัทถือว่าเป็นผู้นำและทำธุรกิจด้วยกันมานานกว่า 20 ปี
- การจัดจำหน่ายรถยนต์ในปัจจุบันมีการชะลอตัวลงค่อนข้างมาก และบริษัทไม่มีความได้เปรียบเหนือศูนย์จำหน่ายรถยนต์อื่นๆทั้งยี่ห้อ Mitsubishi และยี่ห้ออื่นๆภายในพื้นที่
- ราคาก๊าซมีการผันผวนตามราคาน้ำมัน แต่จะช้ากว่า 6 เดือน แต่อย่างไรก็ตามราคาค้าปลีกยังถูกกว่าน้ำมัน 50%


##งบการเงิน

###รายได้

![](./images/18814259_986750154793565_8726820720382701958_n_igxdev.png)

รายได้ของบริษัทมีการผัดเปลี่ยนจากการรับเหมาที่ลดลง แต่มีรายได้จากสถานีบริการที่เพิ่มมากขึ้น ซึ่งมี margin ดีกว่า ปีล่าสุดบริษัทมีการขยายสถานีบริการมากขึ้นทำให้รายได้เติบโตขึ้นเพิ่มกับ margin ที่ดีมากขึ้น และภายในสิ้นปี 2560 ก็จะมีการก่อสร้างเสร็จอีก 3 สาขา จะทำให้บริษัทนั้นมีรายได้ที่เติบโตขึ้นต่อไป
นอกจากการเติบโตจากการขยายธุรกิจแล้ว บริษัทยังมีรายได้มากขึ้นจากการปรับปรุงคุณภาพก๊าซอีกด้วย

ค่าใช่จ่ายในการบริหารเพิ่มขึ้นไม่มากถึงแม้จะมีการขยายสถานีบริการ ซึ่งถือว่าเป็นเรื่องที่ดี

แต่ในส่วนของจำหน่ายรถยนต์นั้นไม่มีการเติบโตซึ่งเป็นสิ่งที่พอจะสามารถคาดการณ์ไว้ล่วงหน้าอยู่แล้ว แต่โดยรวมแล้วหากบริษัทจะเติบโต จะมาจากส่วนของพลังงานเป็นหลัก เนื่องจากเป็นทิศทางของบริษัทประกอบกับธุรกิจส่วนนี้มีกำไรขั้นต้น 25% เมื่อเทียบกับรถยนต์เพียง 10%

![](./images/18881822_986852141450033_1913014242164091604_n_vfbplw.png)

บริษัทมีกำไรที่เติบโตขึ้นในอัตราที่ค่อนข้างสูง แม้ว่าจะตกลงจากปีก่อนหน้าเนื่องจากฐานะที่สูง โดยธุรกิจที่เติบโตนั้นมาจากธุรกิจก๊าซ

![](./images/18835728_986756144792966_1393001168002022571_n_x2mvww.png)

###ฐานะการเงิน

![](./images/18882033_986793738122540_819300277019634316_n_uzj57t.png)

บริษัทมีสินทรัพย์หมุนเวียนค่อนข้างคงที่ในหลายปีที่ผ่านมาเนื่องจากทำธุรกิจที่เกี่ยวข้องกับสินทรัพย์ถาวรเป็นหลัก ซึ่งในส่วนของสินทรัพย์หมุนเวียนดูแล้วไม่มีการเปลี่ยนแปลงมากนัก จึงถือว่าเป็นปกติ โดยส่วนของสินค้าคงคลังจะค่อนข้างสูงคาดว่าจากปริมาณก๊าซและส่วนของรถยนต์

![](./images/18882021_986859024782678_2649887608576326502_n_a9j7t5.png)

สินทรัพย์ถาวรของบริษัทส่วนใหญ่คือที่ดิน อาคาร และอุปกรณ์ต่างๆที่ใช้ดำเนินงานสร้างรายได้ และจะเติบโตขึ้นเพราะมีการขยายธุรกิจ ซึ่งการเติบโตในส่วนนี้มีจากการกู้ ทำให้หนี้สินของบริษัทเพิ่มขึ้นตาม
แต่อย่างไรก็ตามยังมี D/E ที่ไม่เกิน 1 ยังถือว่าอยู่ในเกณฑ์ที่ดี

###กระแสเงินสด

![](./images/18814360_986864258115488_3356418295264973898_n_bfktaz.png)

บริษัทมีเงินสดจากการดำเนินงานค่อนข้างใกล้เคียงกับกำไร

###อัตราส่วน

![](./images/18893169_986864034782177_6180695734687282438_n_zb4qum.png)

ในส่วนของสภาพคล่อง บริษัทมีอัตราส่วนที่ค่อนข้างดี มีวงจรเงินสดเพียงครึ่งเดือนเท่านั้น มีระยะเวลาชำระหนี้ที่นานกว่าระยะเวลาเก็บหนี้
กำไรขั้นต้นและกำไรสุทธิของบริษัทถือว่ารักษามาตรฐานไว้ได้ดี และส่วนของ ROE นั้นจะเห็นว่าตั้งแต่ IPO ทำให้ ROE ตกลงมาอยู่ที 12% แต่ว่าหากบริษัทเริ่มมีรายได้กลับมาจากการลงทุน มีโอกาสกลับขึ้นไปที่ 20% ได้

###สรุป

บริษัทมีการเติบโตที่ยั่งยืนมากขั้นจากการเปลี่ยนจากรับเหมามาเป็นให้บริการด้วยตัวเอง มีการลงทุนขยายธุรกิจอย่างต่อเนื่องทำให้บริษัทมีรายได้และกำไรที่มากขึ้น แนวโน้มของอัตราการใช้ NGV ถึงแม้จะมีการชะลอตัวในช่วงปีที่ผ่านมาจนถึงปัจจุบัน แต่คาดว่าน่าจะเป็นการชะลอตัวชั่วคราว เพราะอย่างไรก็ตาม NGV ก็มีราคาถูกกว่าน้ำมันถึง 50% ซึ่งเป็นเหตุผลที่ดีที่จะช่วยให้อัตราการใช้สามารถเติบโตได้ในระยะยาว ในส่วนของธุรกิจจำหน่ายรถยนต์นั้นคาดว่าน่าจะไม่เติบโตมากนัก เพราะฉะนั้นจึงถือว่าเป็นอีกบริษัทที่น่าติดตามผลการดำเนินงานอย่างยิ่ง

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
