---
title: "ความผิดพลาด 2017"
slug: "/103"
date: "2018-01-09T18:43:50.000Z"
description: "ตอนนี้ก็ปี 2018 แล้ว เวลาผ่านไปเร็วอย่างเหลือเชื่อ เมื่อนึกย้อนกลับไปปีที่แล้ว ผมก็ได้ผ่านเหตุการณ์ต่างๆมามากมายทั้งเรื่องที่ดีและไม่ดี"
image: "./images/sunrise-426085_1280_copy_1_ppxudl.jpg"
featured: false
draft: false
tags: ["การพัฒนาตัวเอง","จิตวิทยาการลงทุน","การลงทุน","แนวคิดการลงทุน"]
---

ตอนนี้ก็ปี 2018 แล้ว เวลาผ่านไปเร็วอย่างเหลือเชื่อ

เมื่อนึกย้อนกลับไปปีที่แล้ว ผมก็ได้ผ่านเหตุการณ์ต่างๆมามากมายทั้งเรื่องที่ดีและไม่ดี

สำหรับการลงทุนก็เช่นกัน ปีที่แล้วถือว่าเป็นปีที่ผมทดลองอะไรหลายๆอย่างมากมาย ผมได้เคยเล่าไปในบทความก่อนหน้านี้แล้ว

ไม่ว่าจะเป็นการทดลองการลงทุนโดยเข้าซื้อหลากหลายรูปแบบ รวมไปถึงกระทั้งการทดลองเข้าซื้อ Warrant อีกด้วย

แม้ว่าผลตอบแทนเมื่อปีที่ผ่านมาอาจจะไม่ได้เป็นไปอย่างที่คาดหวังไว้เพราะเกิดเหตุการณ์ไม่คาดฝันขึ้น แต่ก็ยังถือว่าผมได้มีพัฒนาการที่ดีขึ้นอีกขั้นหนึ่ง

บทเรียนที่ดีที่สุดในการลงทุนสำหรับปีที่แล้วมาจากความผิดพลาดมากมายของผม และสิ่งหนึ่งที่สำคัญคือการที่ผมได้รู้ว่าการเข้าซื้อหุ้นแบบไหนที่ไม่เวิค เพราะอนาคตผมจะได้หลีกเลี่ยงได้ถูกต้อง

ถึงแม้จะผิดพลาดค่อนข้างเยอะในปีที่แล้ว แต่ยังโชคดีที่เมื่อผมถูกต้อง ผลตอบแทนก็สามารถกลบความผิดพลาดของผมได้เยอะพอสมควร

แต่ถ้าจะให้เล่าถึงสิ่งที่พลาดที่สุดในปีที่แล้ว ก็คงจะไม่ใช่เรื่องของการลงทุนที่ผิดพลาด แต่เป็นเรื่องของมุมมองที่ผิดพลาดต่างหาก

ประสบการณ์การลงทุนของผมยังไม่มากนัก ผมจึงค่อนข้างระมัดระวังอย่างมาก โดยเฉพาะการเลือกหุ้น

บริษัทที่ผมเลือกให้ความสนใจส่วนใหญ่จะเป็นบริษัทที่มีผลประกอบการณ์ดีมาเป็นระยะเวลาพอสมควร ซึ่งเป็นสิ่งที่ควรทำอยู่แล้วสำหรับนักลงทุนทุกคน

องค์ประกอบของบริษัทที่ดีในความหมายของผมคือ

1. มี Business model ที่ดี
2. มีความแข็งแรงและมีความสามารถในการแข่งขัน
3. มีผลประกอบการในอดีตที่ดีมาระยะเวลาหนึ่ง

หากวิเคราะห์คร่าวๆแล้วก็จะพบว่าองค์ประกอบข้างต้นนั้นเป็นพื้นฐานของบริษัทที่ดีโดยไม่ต้องสงสัย

แล้วอะไรที่ผิดพลาด?

ความผิดพลาดของผมไม่ใช่การมองผิดว่าบริษัทไหนดีหรือไม่ดี

แต่ความผิดพลาดที่สำคัญที่สุดของผมคือการละเลยบริษัทที่อยู่นอกเหนือองค์ประกอบข้างต้น

การละเลยนั้นทำให้ผมไม่ได้สนใจที่จะศึกษาบริษัทอื่นๆที่เหลือให้มากพอ ซึ่งทำให้พลาดโอกาสดีๆหลายๆครั้ง

ถึงแม้การพลาดโอกาสอาจจะไม่ได้ทำให้ผมสูญเสียเงิน แต่มันทำให้ผมไม่ได้ศึกษาหาความรู้ให้มากขึ้น

ผมคิดว่าการขยายขอบเขตความรู้อย่างสม่ำเสมอเป็นสิ่งที่ทุกคนควรทำ เพราะการรู้เยอะย่อมดีกว่าขาดความรู้

ทั้งนี้ก็ไม่ได้หมายความว่าผมจะพยายามไปลงทุนในสิ่งที่ผมไม่เข้าใจ แต่การพยายามเข้าไปศึกษาและทำความเข้าใจก่อนก็ไม่ได้เสียหายอะไร

เพราะหากเราศึกษาแล้วพบว่าเราไม่เข้าใจเราก็แค่ไม่ต้องลงทุน แต่หากบริษัทนั้นเราสามารถเข้าใจได้แล้วเราไม่ได้ศึกษา นั่นหมายความว่าเราอาจจะเสียโอกาศอย่างจัง

ดังนั้นเมื่อผมพบความผิดพลาดทั้งหมดนี้แล้ว ปีนี้ก็จะเป็นปีที่ผมจะต้องปรับปรุงให้ดีขึ้น

หน้าที่หลักของนักลงทุนไม่ใช่การดูหน้าจอหุ้น แต่เป็นการอ่านและศึกษาหาข้อมูลอย่างสม่ำเสมอ

หากเราไม่หยุดค้นหาโอกาส สักวันเราจะต้องมีโอกาสแน่นอน แต่หากเราหยุดเมื่อไหร่ โอกาสก็ไม่มีทางลอยเข้ามาหาเช่นกัน

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)


