---
title: "ซื้อมือถือ 20,000 ในราคา 7,000"
slug: "/75"
date: "2017-11-27T14:43:21.000Z"
description: "่มือถือกลายเป็นอุปกรณ์ที่ไว้เชื่อมต่อเรากับทั้งโลก หากขาดมือถือไปจะกลายเป็นว่าเราจะตกข่าวสารทุกอย่างโดยทันที"
featured: false
draft: false
tags: ["แนวคิดการลงทุน","Samsung"]
---

ปัจจุบันคงปฏิเสธไม่ได้ว่ามือถือกลายเป็นสิ่งจำเป็นสำหรับเราแล้ว

มือถือกลายเป็นอุปกรณ์ที่ไว้เชื่อมต่อเรากับทั้งโลก หากขาดมือถือไปจะกลายเป็นว่าเราจะตกข่าวสารทุกอย่างโดยทันที

ในแต่ละปีกลายเป็นประเพณีที่บริษัทผู้ผลิต Smart phone จะต้องอัพเดทโมเดลใหม่ การอวดเทคโนโลยีใหม่ๆพร้อมกับการพัฒนาความเร็วในมือถือเป็นสิ่งที่อย่างน้อยจะต้องเกิดขึ้น

ผมคิดว่ามือถือรุ่นใหม่ล่าสุดนั้นดีมาก และผมก็เชื่อว่าทุกๆคนก็ต้องคิดแบบนั้น

แต่ว่ามือถือรุ่นใหม่ราคากลับสูงมาก ตัวอย่าง iPhone X ที่เพิ่งออกล่าสุดก็แพงขึ้นไปอีกกว่าหลักหมื่น

แปลกใจมั้ยว่าในยุคที่ทุกคนต่างบอกว่าข้าวของแพง แต่ Apple กลับขายมือถือในราคาที่แพงขึ้นได้อีก?

ส่วนตัวผมเองก็อยากได้มือถือดีๆใช้เช่นกัน แต่ติดอย่างเดียวตรงที่ราคาแพงเกินไป

ผมหาทางที่จะซื้อมือถือรุ่นท้อปในราคาย่อมเยา และพบว่าหากเราสามารถรอต่อไปได้อีกราว 2 ปี เราสามารถซื้อมือถือรุ่นท้อปได้ในราคาไม่ถึงหมื่น

ผมไม่จำเป็นที่จะต้องได้มือถือที่ดีที่สุด ณ ขณะนั้น เพราะจริงๆแล้วผมรู้ตัวว่าการใช้งานของผมไม่ได้ต้องการเทคโนโลยีใหม่ล่าสุดเสมอ

เมื่อวันก่อนผมซื้อมือถือ Samsung Galaxy S6 มาจาก Aliexpress

มือถือรุ่นนี้เมื่อ 2 ปีที่แล้วเป็นตัวท้อป มีราคาเปิดตัวสูงกว่า 20,000 บาท

แต่มาวันนี้ผมสามารถซื้อได้ในราคาเพียง 7,000 บาทเท่านั้น

หลายๆคนอาจจะคิดว่าการซื้อรุ่นเก่าก็คงจะได้เครื่องช้า แต่ไม่จริงเลย รุ่นท้อปของ 2 ปีที่แล้วยังเร็วกว่ารุ่นใหม่ที่มีราคาปัจจุบันพอๆกัน

แต่ผมจะบอกเคล็ดลับไว้อย่างนึงว่า อย่าได้ลองซื้อสินค้า Apple ตกรุ่น

เพราะ Software ที่ Apple อัพเดทในแต่ละปี จะทำให้รุ่นเก่าๆช้าลงเหมือนตั้งใจวางยา

ที่ผมเขียนเพราะอยากจะเสนออีกทางเลือกในการซื้อมือถือใหม่ให้ทุกคน เราไม่จำเป็นต้องซื้อมือถือใหม่เสมอ แต่รุ่นท้อปของปีก่อนก็ยังเป็นตัวเลือกที่ดีเยี่ยม ประหยัดเงินได้หลักหมื่น

การพยายามใช้เงินทุกบาททุกสตางค์ให้มีค่ามากที่สุด ดูเหมือนผมจะเริ่มมีเลือดแห่ง VI เข้าไปแล้วสิ

และสุดท้ายผมอยากจะบอกว่า ซื้อมือถือจาก Aliexpress ถ้าโชคดีไม่ต้องเสียภาษีนำเข้าด้วย

ปล. ไม่ได้โฆษณา แต่ผมคิดว่ามันน่าสนใจจริงๆ

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)

