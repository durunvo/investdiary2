---
title: "การลงทุนช่วงวิกฤต"
slug: "/170"
date: "2019-01-02T06:19:36.000Z"
description: "วิกฤต คงเป็นสิ่งหนึ่งของธรรมชาติที่ไม่มีใครต้องการให้เกิด เพราะมันมักจะนำพาความเสียหายมาให้แก่ผู้คนจำนวนมาก"
image: "./images/article030-main_1_llkfjw.jpg"
featured: false
draft: false
tags: ["การลงทุน","แนวคิดการลงทุน","Crisis","วิกฤต"]
---

วิกฤต คงเป็นสิ่งหนึ่งของธรรมชาติที่ไม่มีใครต้องการให้เกิด เพราะมันมักจะนำพาความเสียหายมาให้แก่ผู้คนจำนวนมาก

สำหรับตลาดหุ้นเอง ความเสียหายส่วนใหญ่จะอยู่ในเรื่องของความมั่งคั่งส่วนบุคคล บริษัท กองทุน ประเทศ รวมถึงระดับโลก

ดัชนีตลาดหุ้น เป็นดัชนีที่สามารถชี้นำเศรษฐกิจได้มีประสิทธิภาพสูงมาก หากดัชนีกำลังตกลงจนกลายเป็นตลาดหมี นั่นหมายความว่าเศรษฐกิจก็จะกลายเป็นตลาดหมีในอีกไม่นาน หรือหากดัชนีกำลังขึ้นเป็นตลาดกระทิง เศรษฐกิจก็จะกลายเป็นตลาดกระทิงในอีกไม่นานเช่นกัน

สาเหตุเพราะตลาดหุ้นเป็นที่สะสมความมั่งคั่งของผู้คนจำนวนมหาศาลไว้ หากความมั่งคั่งเขาเพิ่มขึ้น เขาก็จะรู้สึกดีและต้องการลงทุนเพิ่มมากขึ้น แต่กลับกันหากความมั่งคั่งของเขาลดลง เขาก็อยากจะชะลอการลงทุน

การเพิ่มการลงทุนทำให้เกิดเงินหมุนเวียนในระบบเศรษฐกิจมากขึ้น การจ้างงานมากขึ้น และเมื่อรายได้ของคนมีมากขึ้น การใช้จ่ายย่อมมีมากขึ้น การใช้จ่ายมากขึ้นย่อมส่งผลต่อไปยังเศรษฐกิจส่วนอื่นๆให้มีการเติบโตมากขึ้น

สิ่งสำคัญการเติบโตนี้มาพร้อมกับหนี้สินในระบบที่เพิ่มมากขึ้น เพราะการเติบโตของธุรกิจย่อมต้องการเงินทุนและการกู้ยืม ส่วนการบริโภคก็ต้องอาศัยเงินกู้เช่นกัน โดยเฉพาะการซื้อบ้านและรถยนต์

แต่การเติบโตย่อมมีขีดจำกัดในช่วงระยะเวลาหนึ่ง โดยเฉพาะการเติบโตจากหนี้สิน เพราะเมื่อหนี้สินจากการขยายธุรกิจก่อตัวขึ้นอย่างรวดเร็ว จนการบริโภคไม่สามารถตามทันได้ ธุรกิจก็ย่อมเกิดการชะลอตัวลงและผลตอบแทนการลงทุนไม่สามารถเป็นไปตามที่คาดการณ์ไว้ได้

ในช่วงชะลอการลงทุน เงินหมุนเวียนในระบบย่อมลดลง ทำให้รายได้คนบางกลุ่มเริ่มฝืด ส่งผลทำให้การบริโภคลดลง การค้าขายก็จะชะลอตัวลง แต่เงินกู้ที่ได้ยืมมาในช่วงเศรษฐกิจดีจะยังต้องชำระคืนอย่างต่อเนื่อง และนี่คือจุดเริ่มต้นของวิกฤตต่างๆ

วิกฤตในมุมมองหนึ่งอาจจะสร้างความเสียหายให้กับระบบเศรษฐกิจ แต่หากมองลึกลงไปเกินกว่านั้นจะพบว่าโอกาสก็สามารถเกิดขึ้นได้เช่นกัน

ดัชนีหุ้นที่ร่วงลงก่อนเกิดวิกฤตอาจจะทำให้นักลงทุนหลายคนมองเป็นโอกาสการลงทุน เพราะนักลงทุนเหล่านั้นจะคาดการณ์โดยอิงจากกำไรของบริษัทในช่วงสภาวะปกติ

ดังนั้นเมื่อราคาหุ้นลดลง แต่กำไรที่คาดการณ์ไว้เท่าเดิม นั่นก็ย่อมเป็นโอกาสทองในการลงทุนหากคาดการณ์ได้ถูกต้อง

แต่แท้จริงแล้วในช่วงที่ดัชนีร่วงลงก่อนเกิดวิกฤตนั้นไม่ได้หมายความบริษัทต่างๆจะไม่ได้รับผลกระทบ เพราะอย่างที่เราได้เรียนรู้มาแล้วว่าผลกระทบมันส่งผลไปในวงกว้างของระบบเศรษฐกิจ

ดังนั้นเป็นไปไม่ได้เลยที่นักลงทุนจะสามารถใช้กำไรในช่วงสภาวะปกติมาคาดการณ์ความถูกหรือความแพงของบริษัทต่างๆในช่วงนี้ และในเมื่อเราไม่อาจรู้ได้ว่าบริษัทใดจะได้รับผลกระทบมากน้อยเพียงใด การคาดการณ์กำไรที่แม่นยำก็ดูเหมือนจะไม่มีประโยชน์เท่าไหร่นัก

ในช่วงวิกฤต สินทรัพย์อย่างหุ้นส่วนใหญ่ล้วนมีราคาถูกกว่าปกติ อย่างแรกนักลงทุนควรมองหาคือบริษัทที่มีโอกาสต่ำมากที่จะได้รับผลกระทบจนบริษัทล้มละลาย หลักการพื้นฐานคือการวิเคราะห์ที่หนี้สินของบริษัทว่าจะต้องไม่มีเงินกู้มากเกินกว่าความสามารถในการชำระคืน

แต่เพียงเท่านี้ยังไม่พอ เพราะต่อให้บริษัทจะไม่ล้มละลาย แต่บางครั้งบริษัทก็อาจจะต้องใช้ความพยายามอย่างมากในการฟื้นฟูธุรกิจจนสามารถกลับมาทำกำไรได้ดีดังเดิม บางครั้งสินค้าหรือบริการของบริษัทไม่ได้เป็นสิ่งที่ตอบสนองพื้นฐานและมีความจำเป็นมากนักสำหรับผู้บริโภค

บริษัทที่มีสินค้าและบริการที่สนองพื้นฐานความต้องการของผู้บริโภคเป็นการลงทุนที่ดีในช่วงเวลานี้ เช่น โรงพยาบาล ร้านค้าปลีก การขนส่ง โรงไฟฟ้า ผู้ให้บริการการสื่อสาร ร้านอาหาร เป็นต้น

มากไปกว่านั้น บางบริษัทอาจมีความเหนือชั้นกว่าด้วยปริมาณเงินสดจำนวนมากที่รอคอยโอกาสในการลงทุน ซึ่งวิกฤตจะทำให้บริษัทเหล่านี้มีเงินสดสามารถนำไปลงทุนได้จำนวนมาก ในช่วงเวลาที่สินทรัพย์ต่างมีราคาไม่แพง รอคอยเพียงการฟื้นฟูของเศรษฐกิจ

ท้ายที่สุดแล้ววิกฤตก็จะคลี่คลายไปได้ เพราะธรรมชาติของมนุษย์จะต้องดิ้นรนเพื่อหาทางรอดอยู่เสมอ และความเสียหายที่ได้เคยเกิดขึ้นก็จะค่อยๆถูกรักษาให้กลับมาแข็งแรงขึ้นเสมอ

ผู้ที่จะชนะในเกมส์นี้จะต้องมองให้ออกว่าเมื่อผ่านพ้นวิกฤตแล้ว บริษัทใดจะสามารถกลับขึ้นมาผงาดได้อีกครั้งและกล้าจะที่ลงทุนเมื่อโอกาสเกิดขึ้น แต่นอกจากความกล้าแล้ว นักลงทุนจะต้องมีความอดทนต่อสภาวะที่ทุกสิ่งล้วนดูแล้วไม่เป็นมิตรต่อการลงทุนด้วย

แต่ช่วงเวลาวิกฤตนี่เองที่เป็นช่วงที่น่าลงทุนที่สุด เพราะวิกฤตมักจะไม่สามารถเกิดขึ้นซ้ำหลังจากวิกฤตได้อีก แต่ในทางกลับกัน วิกฤตส่วนใหญ่มักจะเกิดขึ้นในช่วงเวลาที่ทุกอย่างกำลังบูม ในช่วงเวลาที่ทุกคนต่างมองโลกในแง่ดีและคาดการณ์ทุกอย่างโดยไม่เผื่อความเสี่ยง...

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
