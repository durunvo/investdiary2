---
title: "สรุปงบ Col 3Q2560"
slug: "/98"
date: "2018-01-08T09:14:23.000Z"
description: "ธุรกิจของ COL แบ่งออกเป็น 2 ส่วนหลักๆ 1. OfficeMate ร้านขายเครื่องเขียน อุปกรณ์สำนักงาน 2. B2S ร้านหนังสือและอุปกรณ์เครื่องเขียน"
image: "./images/logo_1_wrj07n.png"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","COL"]
---

ธุรกิจของ COL แบ่งออกเป็น 2 ส่วนหลักๆ

- OfficeMate ร้านขายเครื่องเขียน อุปกรณ์สำนักงาน
- B2S ร้านหนังสือและอุปกรณ์เครื่องเขียน

ปัจจุบันมีร้านค้า 169 สาขา

- Officemate 67 สาขา
- B2S 101 สาขา
- อื่นๆ 1 สาขา

งบ Q3

- ยอดขายรวมโตขึ้น 2.8% YoY
- กำไรขั้นต้นโตขึ้น 9% YoY
- กำไรสุทธิโตขึ้น 45% YoY

มาดูงบตามหมวดหมู่ธุรกิจ

Officemate

- ยอดขายโต 7.6% YoY
- SSSG โต 3.2% รวมสาขาใหม่ที่เปิดด้วย
- รายได้จาก Online โตขึ้น 55% YoY
- รายได้จากโทรศัพลดลง 8%

B2S

- ยอดขายโตขึ้น 1.1% YoY
- SSSG หด -0.9% YoY

โดยรวมดูแล้ว B2S ไม่ค่อยเติบโตนัก แต่อย่างไรก็ตามยังถือว่าเป็นรายได้สำคัญของบริษัท เพราะเป็นสัดส่วนถึง 35%
ส่วน Officemate ก็ยังเติบโตดีอยู่ จากการที่เศรษฐกิจขยายตัว การเปิดสำนักงานก็เติบโตดี

กำไรสุทธิของบริษัทเติบโตอย่างมาก เพราะมีกำไรขั้นต้นที่ดีขึ้น และที่สำคัญคือการยุติธุรกิจออนไลน์ B2C ทำให้ผลขาดทุนไม่ต้องรวมเข้ามาแล้ว (งวดเก้าเดือน ขาดทุนไป 187 ลบ.) แต่หากตัดส่วนนี้ออกไป กำไรสุทธิของ Col ก็ยังเติบโตที่ 6%

อนาคตของบริษัทมีสิ่งที่น่าสนใจ

- พัฒนาบริการ Logistic คลังสินค้าแห่งใหม่ และเริ่มดำเนินการปี 2561
- พัฒนา officemate แบบ franchise ทำให้น่าจะขยายสาขาได้กว้างมากขึ้น และนำผู้ประกอบการต่างๆมาเป็นพัฒนามิตร (น่าสนใจ)
- พัฒนาระบบออนไลน์ให้ Officemate เป็นศูนย์รวม คู่ค้าสามารถเอาของมาลงในระบบได้

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
