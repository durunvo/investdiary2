---
title: "บทเรียนสำคัญกล้องเจ้าปัญหา หุ้น BIG"
slug: "/159"
date: "2018-11-13T18:52:00.000Z"
description: "ตอนที่แล้วได้กล่าวถึงประสบการณ์เกี่ยวกับหุ้น PTG ซึ่งหลังจากนั้นทำให้ผมได้นึกย้อนกลับไปหาบทเรียนจากหุ้น BIG ด้วยเช่นกัน"
image: "./images/Untitled-1_2_y9o2it.png"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","BIG"]
---

ตอนที่แล้วได้กล่าวถึงประสบการณ์เกี่ยวกับหุ้น [PTG](http://investdiary.co/2018/11/10/158/)

ซึ่งหลังจากนั้นทำให้ผมได้นึกย้อนกลับไปหาบทเรียนจากหุ้น BIG ด้วยเช่นกัน

ผมพบว่าจริงๆแล้ว 2 เคสนี้มีความใกล้เคียงกันอย่างมาก ทั้งความผิดพลาดในการดูธุรกิจ และความมีอคติ

จริงๆในตอนหุ้น PTG ยังมีบทเรียนนึงที่ผมไม่ได้กล่าวไว้ ไม่ใช่เพราะว่าไม่อยากพูดถึง แต่พอวิเคราะห์อีกทีก็ยังพบบทเรียนเพิ่มเติมได้อีก

ที่น่าสนใจคือ ประสบการณ์นี้ให้บทเรียนทุกอย่างของหุ้น PTG และ BIG เหมือนกันทุกข้อ

การผิดพลาดซ้ำกันถึงสองรอบเป็นเรื่องที่ไม่ค่อยดีนัก เพราะนั่นหมายถึงเราไม่ได้เรียนรู้จากความผิดพลาด

และต่อไปนี้คือประสบการณ์ส่วนตัวของผมกับหุ้น BIG

บริษัท บิ๊ก คาเมร่า คอร์ปอเรชั่น จำกัด (BIG) ดำเนินธุรกิจขายปลีกกล้องถ่ายรูป โดยมีร้านหลายร้อยสาขาทั่วประเทศชื่อ Big Camera

ในช่วงปี 2557 - 2559 เป็นช่วงพีคของบริษัทเช่นเดียวกับในเคส PTG (น่าแปลกเนอะ)

กำไรสุทธิของบริษัทเติบโตก้าวกระโดดจาก 137 ลบ. เป็น 827 ลบ. และราคาหุ้นเพิ่มขึ้นจาก 1 บาท ขึ้นไปพีคถึง 6 บาท หรือคิดเป็นผลตอบแทนถึง 500% ภายใน 2 ปี

ถึงราคาจะเพิ่มขึ้นมาหลายเท่าตัว แต่เมื่อดู PE ประมาณ 20 เท่าในตอนนั้นก็ถือว่าดูไม่แพงมากกับการเติบโตที่เกิดขึ้น

ในช่วงนั้นกล้อง Mirrorless บูมขึ้น ทำให้ยอดขายเติบโตขึ้นอย่างก้าวกระโดด สาเหตุอาจจะมาจากการที่เป็น Trend ใหม่ของกล้องถ่ายรูปทำให้ผู้คนนิยมในช่วงหนึ่ง

แต่การเติบโตของกล้องนั้นมีจำกัด เพราะมือถือเองก็ได้พัฒนาเทคโนโลยีกล้องจนทำให้ได้ภาพที่สวยไม่แพ้กล้องถ่ายรูป

ในไตรมาส 1 ปี 2560 ราคาหุ้นของ BIG ได้ปรับตัวลดลงจากจุดพีคที่ 6 บาท เหลือเพียงประมาณ 3.6 บาท คิดเป็นการปรับตัวลงกว่า 40% (ลดลงเท่ากับเคส PTG เลย)

สาเหุตของการปรับตัวอย่างรุนแรงของราคาหุ้น มาจากการที่บริษัทมีกำไรสุทธิไตรมาส 1 ไม่เติบโตเมื่อเทียบกับปีก่อน

ในช่วงราคาประมาณ 3.6 บาท เป็นจังหวะที่ผมคิดว่าตลาดตกใจมากเกินไปและพร้อมที่จะให้โอกาสเจ้า BIG ได้แสดงผลงานเติบโตในไตรมาสที่เหลือ

ผมได้เข้าซื้อหุ้นช่วงราคา 3.6 บาท เพราะในช่วงราคานั้น BIG ที่เป็นหุ้นกลุ่มค้าปลีกมี PE เพียง 15 เท่า พร้อมระดับปันผลราวๆ 4% ดูแล้วมีความเสี่ยงต่ำ

หลังจากนั้นไม่นาน งบไตรมาส 2 ก็ได้ประกาศออกมาค่อนข้างน่าผิดหวัง เติบโตติดลบกว่า 6% เมื่อเทียบกับปีก่อน

การเติบโตลดลง 2 ไตรมาศติดถือเป็นสัญญานที่ชัดเจนสำหรับ BIG ว่าได้ผ่านพ้นจุดสูงสุดแล้ว แต่ตัวผมเองก็ยังไม่ยอมแพ้ พร้อมที่จะรอดูไตรมาส 3 เนื่องจากเชื่อว่าจะเข้าสู่ Hi season ตามที่ผู้บริหารได้กล่าวไว้

ผมได้ขายหุ้นออกทันทีหลังจากที่งบไตรมาส 3 ออกมาผิดหวังเช่นเคย ความโชคดีคือผมไม่ได้ขาดทุนมากนัก อาจจะเป็นเพราะว่าได้เข้าซื้อหุ้นในช่วงที่ไม่ได้แพงจนเกินไป

ปัจจุบัน (14 พฤศจิกายน 2561) เวลาผ่านมาปีกว่าๆแล้วทุกอย่างชัดเจนแล้วว่าการตัดสินใจขายหุ้นในตอนนั้นถือเป็นเรื่องที่ประสบความสำเร็จอย่างมาก

ในปี 2561 ผลประกอบการณ์ของ BIG ยังมีแนวโน้มที่ลดลงอย่างต่อเนื่อง ถึงแม้ยอดขายจะเติบโตขึ้น แต่กำไรสุทธิกลับลดลงเพราะ margin ต่ำลง (คิดว่าเป็นเพราะการแข่งขัน)

กำไรสุทธิงวด 9 เดือน

- ปี 2559 - 525 ล้านบาท
- ปี 2560 - 523 ล้านบาท
- ปี 2561 - 418 ล้านบาท

จากราคาหุ้นที่เคยอยู่สูงถึง 6 บาท แต่ปัจจุบันได้ลงมาเหลือเพียง 1.59 บาท เท่านั้น (14 พฤศจิกายน 2561)

การขายหุ้นอย่างเด็ดขาดเมื่อ 1 ปีที่แล้วช่วยรักษาเงินต้นของผมได้อย่างมหาศาล ไม่งั้นปัจจุบันอาจจะขาดทุนไปแล้วกว่า 50%

แล้วบทเรียนในครั้งนี้คืออะไร? จริงๆแล้วไม่ต่างจากเคส PTG

ข้อแรกคือ การอคติในมุมมองต่อหุ้น PE สูงว่าเป็นหุ้นที่ดี และด้วยความโลภทำให้ไม่ประเมินพื้นฐานใหม่เมื่อราคาปรับลดลง

ข้อสองคือ การเข้าใจผิดว่าเป็นหุ้นเติบโต (อีกแล้ว) โดยจริงๆแล้ว BIG ต้องถือเป็นหุ้นประเภทวัฎจักรแฟชั่นต่างหาก อีกทั้งสินค้าของบริษัทก็ไม่ได้แตกต่างจากร้านค้าอื่น ทำให้โดนประเด็นเรื่องการแข่งขันด้านราคาอีกด้วย

ข้อที่สามนี้เป็นข้อสุดท้าย ซึ่งจริงๆแล้วเป็นอีกปัจจัยหนึ่งที่ไม่ได้กล่าวไว้ในเคส PTG  นั่นคือการเชื่อมั่นในเซียนเพียงเพราะเขาได้ซื้อหุ้นไว้เช่นกัน

หลังจากนี้ไป คงต้องเตือนตัวเองให้บ่อยขึ้นว่าห้ามผิดพลาดแบบเดิมอีก สองเคสนี้อาจจะโชคดีที่ขายหุ้นได้ทัน แต่ก็ไม่ได้หมายความว่าเราจะโชคดีแบบนี้เสมอไป

การตัดสินว่าเราถูกหรือผิดควรจะอิงกับผลประกอบการเป็นหลักไม่ใช่ราคาหุ้น ดังนั้นเวลาจะต้องผ่านไปนานพอเพื่อให้ผลประกอบการได้แสดงออกมาก่อนที่เราจะสรุปว่าถูกหรือผิด

ยังเหลือหุ้นอีกหลายตัวที่เจ๊งให้นำมาเล่า ผมเชื่อว่าประสบการณ์จริงแบบนี้จะเป็นประโยชน์ให้กับนักลงทุนจำนวนมาก

จะว่าไปผมก็ไม่ค่อยเห็นใครนำความเจ๊งของตัวเองมาเล่าสักเท่าไหร่ จริงๆความล้มเหลวนี่เป็นประสบการณ์ที่มีประโยชน์มากกว่าความสำเร็จด้วยซ้ำ

ส่วนตอนต่อไปขอไปนอนคิดก่อนว่าจะเล่าถึงหุ้นตัวไหน ไว้คอยติดตามกันได้ครับ

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
