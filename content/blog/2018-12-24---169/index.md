---
title: "ภาพรวมกลุ่มธุรกิจ 9M2561"
slug: "/169"
date: "2018-12-24T08:13:04.000Z"
description: "รวมสรุปภาพรวมกลุ่มธุรกิจ 9M2561"
image: "./images/compareindustry.001_1_hhoqt5.jpg"
featured: false
draft: false
tags: ["Economy","การลงทุน"]
---

###หุ้นโรงพยาบาล
![เปรียบเทียบหุ้นโรงพยาบาล](./images/compareindustry.001_rlkksg.jpg)

###หุ้นธนาคาร
![เปรียบเทียบหุ้นธนาคาร](./images/compareindustry.002_g3h8k4.jpg)

###หุ้นสายการบิน
![เปรียบเทียบหุ้นสายการบิน](./images/compareindustry.003_al9bun.jpg)

###หุ้นโบรกเก้อ
![เปรียบเทียบหุ้นโบรกเก้อ](./images/compareindustry.005_nqbig5.jpg)

###หุ้นโรงไฟฟ้า
![เปรียบเทียบหุ้นโรงไฟฟ้า](./images/compareindustry.006_v8s4oe.jpg)

###หุ้นความงาม
![เปรียบเทียบหุ้นความงาม](./images/compareindustry.007_ww7oxq.jpg)

###หุ้นร้านอาหาร
![เปรียบเทียบหุ้นร้านอาหาร](./images/compareindustry.011_kyimrw.jpg)

###หุ้นอสังหาฯ
![เปรียบเทียบหุ้นอสังหาฯ](./images/compareindustry.001_z9noee.jpg)

###หุ้นโรงกลั่น
![เปรียบเทียบหุ้นโรงกลั่น](./images/compareindustry.009_f2dnf0.jpg)

###หุ้นสินค้าอาหาร
![เปรียบเทียบหุ้นสินค้าอาหาร](./images/compareindustry.001_y05wz7.jpg)

---

ติดตามแบ่งปันความรู้การลงทุนได้ที่ [Investdiary](https://fb.com/investdiary)
