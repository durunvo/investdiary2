---
title: "Major Cineplex ท่ามกลางการ disrupt จากเทคโนโลยี"
slug: "/144"
date: "2018-06-10T00:29:09.000Z"
description: "ในช่วงหลายปีที่ผ่านมานี้ เทคโนโลยีต่าง ๆ ในยุคปัจจุบันนั้นมีการเปลี่ยนแปลงที่รวดเร็วอย่างไม่น่าเชื่อ"
image: "./images/S__301878741.jpg"
featured: false
draft: false
tags: ["หุ้นรายตัว","การลงทุน","Major"]
---

ในช่วงหลายปีที่ผ่านมานี้ เทคโนโลยีต่าง ๆ ในยุคปัจจุบันนั้นมีการเปลี่ยนแปลงที่รวดเร็วอย่างไม่น่าเชื่อ หากเราสามารถย้อนกลับไปเพียง 10 ปีที่แล้วเราอาจจะแปลกใจว่าสังคมมีความต่างกันอย่างสิ้นเชิง

หนึ่งในเทคโนโลยีที่เปลี่ยนแปลงผู้คนอย่างมากก็คือความรวดเร็วในการสื่่อสาร

ผมคิดว่าการสื่อสารในยุคปัจจุบันนั้นมีความรวดเร็วมากกว่าเมื่อ 10 ปีที่แล้วถึงหลายสิบเท่าตัว และหากย้อนกลับไปเพียง 20 ปีที่แล้ว อาจจะเร็วกว่าถึงร้อยเท่าตัว

ความรวดเร็วในการสื่อสารนี้ก่อให้เกิดโอกาสมากมายสำหรับผู้ที่มองเห็นโอกาส แต่กลับกันก็สามารถเป็นตัวทำลายของผู้ที่ปรับตัวไม่ทันได้เช่นกัน

การดู Video ผ่าน Online เป็นหนึ่งในธุรกิจได้ผลประโยชน์อย่างมากจากการพัฒนาในความเร็วของการสื่อสาร ไม่ว่าจะเป็น Platform อย่าง Youtube, Netflix หรือ iflix

แน่นอนว่าก็จะต้องมีธุรกิจที่ได้รับผลกระทบเช่นกัน ธุรกิจสื่ออย่างโทรทัศน์เป็นตัวอย่างที่ชัดเจนที่สุด

เมื่อสมัยก่อนโทรทัศน์เป็นช่องทางที่มีการเข้าถึงของคนจำนวนมากที่สุด ดังนั้นธุรกิจช่องโทรทัศน์จึงสามารถสร้างผลตอบแทนได้อย่างมหาศาล

แต่เมื่อมีการเกิดขึ้นของสื่อ Online ทำให้ผู้คนหันจากการดูโทรทัศน์ไปยัง Online มากขึ้น และแน่นอนว่าเรตติ้งของช่องโทรทัศน์ก็จะน้อยลงตามไปด้วย

รายได้หลักของช่องโทรทัศน์มาจากโฆษณาเป็นหลัก ยิ่งรายการมีเรตติ้งดีเท่าไหร่ ก็จะมีผู้สนใจมาโฆษณาในช่วงเวลานั้นมากขึ้น และแน่นอนว่าค่าโฆษณาก็จะแพงขึ้นด้วย

การที่เปลี่ยนมาเป็นแบบ Online ทำให้การโฆษณาจากช่องทางเดิมทำได้ยากขึ้น ความมั่งคั่งของช่องโทรทัศน์จึงลดลงอย่างเห็นได้ชัดเมื่อเทียบกับในอดีต  ดังที่เราเห็นราคาหุ้นของธุรกิจโทรทัศน์ลดลงอย่างมากมาย

ยังมีธุรกิจอื่นอีกหลาย ๆ ธุรกิจที่ได้รับผลกระทบจากการปรับตัวไม่ทันเมื่อเทคโนโลยีใหม่เข้ามา disrupt ซึ่งธุรกิจที่น่าสนใจมากในมุมมองของผมก็อาจจะเป็น “โรงภาพยนตร์“

ในประเทศไทย มีผู้เล่นในธุรกิจโรงภาพยนตร์อยู่สองเจ้าหลัก ๆ คือ Major Cineplex ซึ่งเป็นผู้นำ และที่สองรองลงมาคือ SF

เราลองมาศึกษาธุรกิจของ Major Cineplex กันว่าในภาวะการเปลี่ยนแปลงทางเทคโนโลยีแบบนี้ บริษัทมีจะมีแนวโน้มเป็นอย่างไร

ปัจจุบัน Major Cineplex มีธุรกิจหลายประเภท โดยโรงภาพยนตร์ถือเป็นธุรกิจหลัก แต่นอกจากนี้ก็ยังมี ธุรกิจลานโบว์ลิ่ง คาราโอเกะ ลานสเก็ตน้ำแข็ง อสังหาริมทรัพย์ และศูนย์สุขภาพ

ข้อมูล ปี 2561 เครือ Major มีสาขารวมทั้งสิ้น 131 สาขา 710 โรงภาพยนตร์ ซึ่งในนี้มีสาขาในประเทศกัมพูชาจำนวน 3 สาขา และ ลาว 2 สาขา

หากเทียบกับโรงภาพยนต์ในเครือ SF แล้ว Major Cineplex ถือว่ามีจำนวนโรงมากกว่าประมาณถึง 1 เท่าตัว

หลาย ๆ คนอาจจะคิดว่าธุรกิจโรงภาพยนตร์นั้นน่าจะไม่ค่อยเติบโต เพราะพฤติกรรมผู้บริโภคนั้นมีแนวโน้มหันไปดูผ่านทาง Online มากขึ้น

แต่หากเรามีดูตัวเลขจำนวนตั๋วที่ Major Cineplex ขายต่อปีเราจะพบกับความขัดแย้งในสมมติฐานข้างต้น ซึ่งในปี 2559 Major ขายตั๋วหนังอยู่ที่ 29.5 ล้านใบ และในปี 2560 มีจำนวน 31 ล้านใบ

แน่นอนว่าการมาของเทคโนโลยี อาจจะทำให้พฤติกรรมผู้บริโภคหันไปดูหนัง Online มากขึ้น แต่ก็อาจจะไม่กระทบกับธุรกิจโรงภาพยนตร์มากนัก

เหตุผลแรกคือ โรงภาพยนตร์เป็นกิจกรรมนอกบ้านอย่างหนึ่ง เช่นเดียวกับการทานอาหารนอกบ้าน หรือการออกเที่ยวต่างจังหวัด

แน่นอนว่าในวันนี้ทุกคนสามารถสั่งอาหารทานที่บ้านหรือดูหนังที่บ้านได้ แต่ทั้งหมดนี้ก็ไม่สามารถทดแทนประสบการณ์นอกบ้านได้ดีนัก

เหตุผลที่สองคือ การเติบโตของธุรกิจภาพยนตร์ถือเป็นปัจจัยสำคัญของโรงภาพยนตร์

ในยุคปัจจุบันถือเป็นยุคที่เฟื่องฟูในเรื่องการผลิตภาพยนตร์อย่างมาก ซึ่งเหตุผลที่สำคัญก็อาจจะมาจากเทคโนโลยีที่ก้าวหน้ามาก จนทำให้จินตนาการของผู้ผลิตภาพยนตร์สามารถทำเป็นรูปธรรมได้ไม่ยาก

หากเราดูข้อมูลภาพยนตร์ที่ทำรายได้สูงสุดตลอดกาล ในภาพยนต์ 20 อันดับแรก มีเพียง 2 เรื่องเท่านั้นที่ไม่ได้ผลิตภายในช่วง 10 ปีที่ผ่านมา นั้นก็คือ Titanic (1997) และ The Lord of the Rings (2003)

ที่น่าสนใจไปกว่านั้นคือในจำนวน 20 อันแรกนี้ มีภาพยนต์ถึง 9 เรื่องที่เป็นของบริษัทผลิต Content รายใหญ่อย่าง Disney

นี่เป็นหลักฐานที่ยืนยันได้อย่างดีว่า ยุคนี้คือยุคที่รุ่งเรืองของวงการภาพยนตร์อย่างมาก

รายได้ส่วนใหญ่ของผู้ผลิตภาพยนตร์นั้นมาจากโรงภาพยนตร์เป็นหลัก เนื่องจาก Business model นั้นจะเป็นการแบ่งรายได้กัน

ดังนั้นในเมื่อโรงภาพยนตร์มีส่วนสำคัญอย่างมากต่อการเติบโตของวงการภาพยนต์ ผู้ผลิตภาพยนตร์จึงให้สิทธื์ในการฉายแก่โรงภาพยนต์ก่อนเสมอ เหนือ platform online ต่าง ๆ

.

มาดูรายได้ของ Major กันบ้าง

ปี 2557 – รายได้รวม 9,053 ลบ. กำไรสุทธิ 1,086 ลบ.
ปี 2558 – รายได้รวม 9,201 ลบ. กำไรสุทธิ 1,170 ลบ.
ปี 2559 – รายได้รวม 9,580 ลบ. กำไรสุทธิ 1,188 ลบ.
ปี 2560 – รายได้รวม 9,937 ลบ. กำไรสุทธิ 1,193 ลบ.

จริง ๆ แล้วบริษัท Major มีการเติบโตขึ้นในทุก ๆ ปี ซึ่งกลยุทธ์หลักที่ใช้ก็คือการขยายสาขาเป็นหลัก รวมถึงมูลค่าโฆษณาผ่านโรงภาพยนตร์ก็มีการเติบโตขึ้นด้วย

ในปี 2561 นี้เองก็มีภาพยนต์ที่น่าสนใจหลาย ๆ เรื่องเข้าฉายไม่ว่าจะเป็นภาพยนต์ฟอร์มยักษ์จากฮอลลิวู้ด ภาพยนตร์ในเอเชีย และภาพยนตร์จากไทย

หลายคนอาจจะยังไม่รู้ว่าจริง ๆ แล้วธุรกิจโรงภาพยนต์ Major ในปัจจุบันยังมีอัตราการเข้าถึงของคนต่างจังหวัดน้อยอยู่มาก

พฤติกรรมการดูภาพยนตร์ของคนกรุงเทพ และต่างจังหวัดจะต่างกันโดยสิ้นเชิง คนกรุงเทพจะเน้นภาพยนตร์ฮอลลิวู้ด ในขณะที่ต่างจังหวัดจะเน้นภาพยนตร์ของไทยมากกว่า

ภาพยนตร์ของไทยที่ดี ๆ ยังมีจำนวนไม่มาก เรื่องนี้ส่งผลอย่างมากต่อจำนวนคนที่ดูภาพยนตร์ตามต่างจังหวัด ซึ่งเป็นสัดส่วนประชากรที่เยอะกว่าคนกรุงเทพมากนัก

หากอุตสาหกรรมภาพยนตร์ของประเทศไทยมีพัฒนาการที่ดีและมีการเติบโตที่ดีขึ้น จะเป็นส่วนสำคัญอย่างมากที่จะทำให้ธุรกิจโรงภาพยนตร์ยังเติบโตต่อไปได้ โดยเฉพาะคนต่างจังหวัด

ผมเองก็หวังว่าอุตสาหกรรมภาพยนตร์ของไทยจะเติบโตจนสามารถกลายเป็นสินค้าส่งออกได้ เหมือนอย่างที่ภาพยนตร์เรื่อง ฉลาดเกมส์โกง (Bad genius) ประสบความสำเร็จอย่างมากในต่างประเทศ

สรุปแล้ว Major Cineplex อาจจะยังไม่ได้รับผลกระทบจากการเปลี่ยนแปลงทางเทคโนโลยีมากนักในช่วงนี้ เนื่องจาก Content ที่สร้างรายได้มหาศาลอย่างภาพยนตร์ยังอยู่ในช่วงขาขึ้น และโรงภาพยนตร์ยังถือว่าเป็น Entertainment นอกบ้านของคนไทย

แต่ในขณะเดียวกันความไม่ประมาทก็เป็นสิ่งสำคัญ เพราะ Platform อื่น ๆ อย่าง Netflix ก็ได้พยายามสร้าง Content ขึ้นมาเองเช่นกัน

เมื่อไหร่ก็ตามที่ความนิยมของ Content ได้ถูกเปลี่ยนจากภาพยนตร์ไปเป็นรูปแบบอื่น Major Cineplex อาจจะต้องปรับเปลี่ยนตัวเองครั้งใหญ่เพื่อความอยู่รอด

---

Credit: http://guawesome.com/blog/2018/05/09/major-cineplex-ท่ามกลางการ-disrupt-จากเทคโน/
