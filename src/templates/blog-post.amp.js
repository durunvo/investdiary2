import React from 'react'
import { graphql } from 'gatsby'
import { transformGhostPath } from '../utils/pathname'
import SEO from '../components/seo'
import Image from 'gatsby-image'

class BlogPostTemplate extends React.Component {

  render() {
    const { data } = this.props
    const post = data.markdownRemark
    const { author } = data.site.siteMetadata

    return (
      <section id="post-page">
        <SEO
          amp
          article={post}
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
          slug={transformGhostPath(post.fields.slug)}
          image={post.frontmatter.image && post.frontmatter.image.publicURL}
        />
        <h1>
          {post.frontmatter.title}
        </h1>
        <p>
          by {author}
        </p>
        <p>
          {post.frontmatter.formattedDate}
        </p>
        {post.frontmatter.image && <Image fluid={post.frontmatter.image.childImageSharp.fluid} />}

        {/* {post.frontmatter.image && (
          <amp-img
            src={post.frontmatter.image.childImageSharp.fluid.src}
            src-set={post.frontmatter.image.childImageSharp.fluid.srcSet}
            width={post.frontmatter.image.childImageSharp.resolutions.width}
            height={post.frontmatter.image.childImageSharp.resolutions.height}
            layout="responsive"
          />
        )} */}
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </section>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query ($slug: String!) {
    site {
      siteMetadata {
        siteUrl
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      fields {
        slug
      }
      frontmatter {
        title
        date
        formattedDate: date(formatString: "MMMM DD, YYYY")
        description
        tags
        image {
          publicURL
          childImageSharp {
            resolutions {
              width
              height
            }
            fluid(maxWidth: 1000, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
