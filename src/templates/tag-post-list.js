import React from 'react'
import { graphql } from 'gatsby'
import BlogPostList from '../components/blogpostlist'
import SEO from '../components/seo'
import { getTagSlug } from '../utils/pathname'

class BlogIndex extends React.PureComponent {
  render() {
    const { data, pageContext } = this.props
    const posts = data.allMarkdownRemark.edges
    const { currentPage, totatPosts, tag } = pageContext

    return (
      <>
        <SEO title={tag.toUpperCase()} slug={`/tag/${getTagSlug(tag)}`}/>
        {/* <Bio /> */}
        <h2>Tag - {tag.toUpperCase()}</h2>
        <BlogPostList
          basename={`/tag/${getTagSlug(tag)}`}
          posts={posts}
          currentPage={currentPage}
          totatPosts={totatPosts}
        />
      </>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query ($skip: Int!, $limit: Int!, $tag: String) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } }, fileAbsolutePath: { regex: "/blog/" } }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date
            formattedDate: date(formatString: "MMMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fluid(maxWidth: 1000, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
