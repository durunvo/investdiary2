import React from 'react'
import { graphql } from 'gatsby'
import Bio from '../components/bio'
import BlogPostList from '../components/blogpostlist'
import SEO from '../components/seo'

class BlogIndex extends React.PureComponent {
  render() {
    const { data, pageContext } = this.props
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMarkdownRemark.edges
    const { currentPage, totatPosts } = pageContext
    
    return (
      <>
        <SEO title={siteTitle} slug={`/page/${currentPage}`}/>
        {/* <Bio /> */}
        <BlogPostList posts={posts} currentPage={currentPage} totatPosts={totatPosts}/>
      </>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query ($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date
            formattedDate: date(formatString: "MMMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fluid(maxWidth: 1000, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
