import React from 'react'
import { Link, graphql } from 'gatsby'
import { rhythm, scale } from '../utils/typography'
import { transformGhostPath, getTagSlug } from '../utils/pathname'
import { Row, Col, Tag } from 'antd'
import Image from 'gatsby-image'
import Bio from '../components/bio'
import SEO from '../components/seo'
import AdSense from '../components/adsense'
import PostCard from '../components/postcard'

class BlogPostTemplate extends React.Component {

  render() {
    const { data, pageContext, path } = this.props
    const post = data.markdownRemark
    const { siteUrl } = data.site.siteMetadata
    const nextArticle = data.nextArticle
    const previousArticle = data.previousArticle

    return (
      <section id="post-page">
        <SEO
          article={post}
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
          slug={transformGhostPath(post.fields.slug)}
          image={post.frontmatter.image}
        />
        <h1
          style={{
            marginTop: rhythm(1),
            marginBottom: 0,
          }}
        >
          {post.frontmatter.title}
        </h1>
        <p
          style={{
            ...scale(-1 / 5),
            display: `block`,
            marginBottom: rhythm(1),
          }}
        >
          {post.frontmatter.formattedDate}
        </p>
        {post.frontmatter.image && <Image fluid={post.frontmatter.image.childImageSharp.fluid} />}
        <br/>
        <AdSense pathname={path}/>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
        <div>
          Tags: {
            post.frontmatter.tags.map((tag, index) =>
              <Tag key={index} color="blue"><a href={`/tag/${getTagSlug(tag)}`}>{tag}</a></Tag>
            )
          }
        </div>
        <hr style={{ margin: `${rhythm(1)} 0` }} />
        <Bio />
        <div className="fb-comments" data-href={`${siteUrl}${transformGhostPath(post.fields.slug)}`} data-numposts="5"></div>
        <Row type="flex" justify="space-between" gutter={12} style={{ margin: `${rhythm(1)} 0 0` }}>
          {previousArticle && (
            <Col xs={24} sm={12}>
              <PostCard post={previousArticle} />
            </Col>
          )}
          {nextArticle && (
            <Col xs={24} sm={12}>
              <PostCard post={nextArticle} />
            </Col>
          )}
        </Row>
        {/* <ul
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            listStyle: 'none',
            padding: 0,
            margin: `0 0 ${rhythm(1)}`
          }}
        >
          <li>
            {previous && (
              <a href={transformGhostPath(previous.fields.slug)} rel="prev">
                ← {previous.frontmatter.title}
              </a>
            )}
          </li>
          <li>
            {next && (
              <a href={transformGhostPath(next.fields.slug)} rel="next">
                {next.frontmatter.title} →
              </a>
            )}
          </li>
        </ul> */}
      </section>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query ($slug: String!, $next: String, $previous: String) {
    site {
      siteMetadata {
        siteUrl
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      fields {
        slug
      }
      frontmatter {
        title
        date
        formattedDate: date(formatString: "MMMM DD, YYYY")
        description
        tags
        image {
          publicURL
          childImageSharp {
            resolutions {
              width
              height
            }
            fluid(maxWidth: 1000, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    nextArticle: markdownRemark(fields: { slug: { eq: $next } }) {
      id
      excerpt(pruneLength: 160)
      html
      fields {
        slug
      }
      frontmatter {
        title
        date
        formattedDate: date(formatString: "MMMM DD, YYYY")
        description
        tags
        image {
          childImageSharp {
            fluid(maxWidth: 1000, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    previousArticle: markdownRemark(fields: { slug: { eq: $previous } }) {
      id
      excerpt(pruneLength: 160)
      html
      fields {
        slug
      }
      frontmatter {
        title
        date
        formattedDate: date(formatString: "MMMM DD, YYYY")
        description
        tags
        image {
          childImageSharp {
            fluid(maxWidth: 1000, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
