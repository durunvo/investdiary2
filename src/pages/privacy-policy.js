import React from 'react'
import { graphql } from 'gatsby'
import SEO from '../components/seo'

class PrivacyPolicy extends React.PureComponent {
  render() {
    const { data } = this.props
    const post = data.markdownRemark

    return (
      <>
        <SEO title={post.frontmatter.title} description={post.excerpt} slug="/privacy-policy" />
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </>
    )
  }
}

export default PrivacyPolicy

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fileAbsolutePath: {regex: "/privacy-policy.md/"}) {
      id
      html
      excerpt
      fields {
        slug
      }
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
      }
    }
  }
`
