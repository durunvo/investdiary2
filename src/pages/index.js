import React from 'react'
import { graphql } from 'gatsby'
import BlogPostList from '../components/blogpostlist'
import SEO from '../components/seo'
import Bio from '../components/bio'

class BlogIndex extends React.PureComponent {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMarkdownRemark.edges
    return (
      <>
        <SEO title="หน้าหลัก" />
        <Bio />
        <BlogPostList posts={posts} currentPage={1} totatPosts={data.allMarkdownRemark.totalCount}/>
      </>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      limit: 6
      skip: 0
    ) {
      totalCount
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date
            formattedDate: date(formatString: "MMMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fluid(maxWidth: 1000, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
