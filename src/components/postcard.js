import React, { memo } from 'react'
import { Link } from 'gatsby'
import { rhythm } from '../utils/typography'
import { transformGhostPath } from '../utils/pathname'
import Image from 'gatsby-image'

export default memo(({ post }) => {
  const title = post.frontmatter.title || post.fields.slug
  return (
    <div style={{
      borderRadius: 2,
      marginBottom: rhythm(1),
      boxShadow: '0px 0px 2px 0px',
    }}>
      <a style={{ boxShadow: `none` }} href={transformGhostPath(post.fields.slug)}>
      <div className="sixteen-nine">
        {post.frontmatter.image && <Image className="content" fluid={post.frontmatter.image.childImageSharp.fluid} />}
      </div>
      </a>
      <div style={{ padding: rhythm(0.5) }}>
        <h3 style={{ fontSize: rhythm(0.6), marginBottom: 0 }}>
          <a style={{ boxShadow: `none` }} href={transformGhostPath(post.fields.slug)}>
            {title}
          </a>
        </h3>
        <small>{post.frontmatter.formattedDate}</small>
        <p
          style={{ fontSize: rhythm(0.5), marginBottom: 0 }}
          dangerouslySetInnerHTML={{
            __html: post.frontmatter.description || post.excerpt,
          }}
        />
        <div style={{ textAlign: 'end', fontSize: rhythm(0.5) }}><a href={transformGhostPath(post.fields.slug)}>อ่านเพิ่ม →</a></div>
      </div>
    </div>
  )
})
