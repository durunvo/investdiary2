import React, { memo, useEffect } from 'react'

export default memo(({ pathname }) => {
  useEffect(() => {
    (window.adsbygoogle = window.adsbygoogle || []).push({});
  }, [pathname])
  return (
    <section className="ads-banner" style={{ justifyContent:'space-between',margin:'0 auto',padding:'1vw 0 2vw' }}>
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <ins className="adsbygoogle"
        style={{ display:'block', textAlign:'center' }}
        data-ad-layout="in-article"
        data-ad-format="fluid"
        data-ad-client="ca-pub-7465516195173372"
        data-ad-slot="3784904093"></ins>
    </section>
  )
})
