import React, { memo } from 'react'
import { Link, navigate } from 'gatsby'
import { Row, Col, Pagination } from 'antd'
import PostCard from './postcard'

export default memo(({ basename = '', posts, currentPage, totatPosts }) => {
  const itemRender = (current, type, originalElement) => {
    if (type === 'page') {
      if (current === 1) return <a href="/">{current}</a>
      return <a href={`${basename}/page/${current}`}>{current}</a>
    }
    return originalElement
  }

  return (
    <>
      <Row type="flex" gutter={12}>
        {
          posts.map(({ node }, index) => {
            return (
              <Col xs={24} sm={12} key={index}><PostCard post={node}/></Col>
            )
          })
        }
      </Row>
      <Pagination
        hideOnSinglePage
        total={totatPosts}
        pageSize={6}
        showTotal={(total, range) => `${range[0]}-${range[1]} จาก ${total}`}
        current={currentPage}
        itemRender={itemRender}
        onChange={page => {
          window.location.push(page === 1 ? `/${basename}` : `${basename}/page/${page}`)
        }}
      />
      {
        // !!previousPage && (
        //   <div><a href={previousPage}>Back</a></div>
        // )
      }
      {
        // !!nextPage && (
        //   <div><a href={nextPage}>Next</a></div>
        // )
      }
    </>
  )
})
