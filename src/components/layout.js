import React from 'react'
import { useStaticQuery, graphql, Link } from 'gatsby'
import { rhythm, scale } from '../utils/typography'
import { Row, Col } from 'antd'
import FacebookPage from './facebookpage'
import Image from 'gatsby-image'

const Layout = ({ data, location, children, path }) => {

  const { avatar } = useStaticQuery(
    graphql`
      query {
        avatar: file(absolutePath: { regex: "/profile-pic.png/" }) {
          childImageSharp {
            fixed(width: 50, height: 50) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    `
  )
  
  const siteTitle = data.site.siteMetadata.title
  const rootPath = `${__PATH_PREFIX__}/`
  let header

  if (location.pathname === rootPath) {
    header = (
      <h1
        style={{
          ...scale(1.1),
          marginBottom: rhythm(1.1),
          marginTop: 0,
        }}
      >
        <a
          style={{
            boxShadow: `none`,
            textDecoration: `none`,
            color: `inherit`,
          }}
          href="/"
        >
          {siteTitle}
        </a>
      </h1>
    )
  } else {
    header = (
      <h3
        style={{
          // fontFamily: `Montserrat, sans-serif`,
          marginTop: 0,
        }}
      >
        <a
          style={{
            boxShadow: `none`,
            textDecoration: `none`,
            color: `inherit`,
          }}
          href="/"
        >
          <Row type="flex" align="middle" gutter={12}>
            <Col>
            <Image
              fixed={avatar.childImageSharp.fixed}
            />
            </Col>
            {siteTitle}
          </Row>
        </a>
      </h3>
    )
  }

  return (
    <div
      style={{
        marginLeft: `auto`,
        marginRight: `auto`,
        maxWidth: rhythm(24),
        padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`,
      }}
    >
      <header>{header}</header>
      <main>{children}</main>
      <footer style={{ marginTop: rhythm(1.5) }}>
        <Row type="flex" gutter={12} justify="space-between">
          <Col>
            <div style={{ marginBottom: rhythm(0.5) }}><FacebookPage pathname={path}/></div>
          </Col>
          <Col>
            <ul>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/">หน้าแรก</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/tag/startinvest">เริ่มต้นลงทุน</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/tag/wiekhraaah-utsaahkrrm">วิเคราะห์อุตสาหกรรม</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/tag/4">แนวคิดการลงทุน</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/tag/8">การประเมินมูลค่า</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/tag/aswath-damodaran">Aswath Damodaran</a></li>
              <li style={{ marginBottom: rhythm(0.1) }}><a href="/privacy-policy">Privacy Policy</a></li>
            </ul>
          </Col>
        </Row>
        <div style={{ marginTop: rhythm(0.5) }}><small>
          © {new Date().getFullYear()}, built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
          {` `}
          and developed by <a href="https://krithoonsuwan.netlify.com">Krit Hoonsuwan</a>
        </small></div>
      </footer>
    </div>
  )
}

export default Layout
