import React, { memo, useEffect } from 'react'

export default memo(({ pathname }) => {
  // useEffect(() => {
  //   if (window.FB) {
  //     // Read the entire document for `fb-*` classnames
  //     console.log('FB.XFBML.parse');
  //     window.FB.XFBML.parse();
  //   }
  // }, [pathname])
  return (
    <div className="fb-page"
      data-href="https://www.facebook.com/investdiary"
      data-adapt-container-width="true"
      data-hide-cover="false"
      data-show-facepile="true"></div>
  )
})
