/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'
import { websiteJsonld, articleJsonld } from '../utils/seo'

function SEO({ description, lang, title, slug, article, amp, image }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            siteUrl
            title
            description
            author
            social {
              facebook
            }
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description

  const link = [{
    rel: 'canonical',
    href: `${site.siteMetadata.siteUrl}${slug}`
  }]
  if (amp) {
    link.push({
      rel: 'amphtml',
      href: `${site.siteMetadata.siteUrl}${slug}amp/`
    })
  }
  const _meta = [
    {
      name: 'HandheldFriendly',
      content: 'True',
    },
    {
      name: 'referrer',
      content: 'no-referrer-when-downgrade',
    },
    {
      name: `description`,
      content: metaDescription,
    },
    {
      property: 'og:site_name',
      content: title,
    },
    {
      property: `og:title`,
      content: title,
    },
    {
      property: `og:description`,
      content: metaDescription,
    },
    {
      property: 'og:url',
      content: `${site.siteMetadata.siteUrl}${slug}`,
    },
    {
      name: `twitter:card`,
      content: `summary_large_image`,
    },
    {
      name: `twitter:creator`,
      content: site.siteMetadata.author,
    },
    {
      name: `twitter:title`,
      content: title,
    },
    {
      name: `twitter:description`,
      content: metaDescription,
    },
    {
      name: `twitter:url`,
      content: `${site.siteMetadata.siteUrl}${slug}`,
    },
    {
      name: `fb:pages`,
      content: '819249924876923',
    }
  ]
  if (image) {
    _meta.push({
      property: 'twitter:image',
      content: `${site.siteMetadata.siteUrl}${image.publicURL}`,
    })
    _meta.push({
      property: 'og:image',
      content: `${site.siteMetadata.siteUrl}${image.publicURL}`,
    })
    if (image.childImageSharp) {
      _meta.push({
        property: 'og:image:width',
        content: image.childImageSharp.resolutions.width,
      })
      _meta.push({
        property: 'og:image:height',
        content: image.childImageSharp.resolutions.height,
      })
    }
  }
  if (article) {
    _meta.push({
      property: 'og:type',
      content: 'article',
    })
    _meta.push({
      property: 'article:published_time',
      content: article.frontmatter.date,
    })
    _meta.push({
      property: 'article:modified_time',
      content: article.frontmatter.date,
    })
    _meta.push({
      property: 'twitter:label1',
      content: 'Written by',
    })
    _meta.push({
      property: 'twitter:data1',
      content: site.siteMetadata.author,
    })
    _meta.push({
      property: 'twitter:label2',
      content: 'Filed under',
    })
    _meta.push({
      property: 'twitter:data2',
      content: article.frontmatter.tags.join(', '),
    })
    article.frontmatter.tags.forEach(tag => {
      _meta.push({
        property: 'article:tag',
        content: tag,
      })
    })
    _meta.push({
      property: 'article:publisher',
      content: `https://www.facebook.com/${site.siteMetadata.social.facebook}`,
    })
  } else {
    _meta.push({
      property: `og:type`,
      content: 'website',
    })
  }

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      link={link}
      meta={_meta}
    >
      <script type="application/ld+json">
        {JSON.stringify(
          article ? articleJsonld(site, article.frontmatter) : websiteJsonld(site)
        )}
      </script>
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bd94d18344069ad"></script>
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `th`,
  meta: [],
  description: ``,
  slug: '/',
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
