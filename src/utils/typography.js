import Typography from 'typography'
import Theme from 'typography-theme-judah'

// Theme.overrideThemeStyles = () => {
//   return {
//     "a.gatsby-resp-image-link": {
//       boxShadow: `none`,
//     },
//     "#post-page .gatsby-image-wrapper": {
//       marginBottom: '20px',
//     },
//     "a": {
//       color: `#007acc`,
//     }
//   }
// }

// delete Theme.googleFonts

const typography = new Typography(Theme)

// Hot reload typography in development.
if (process.env.NODE_ENV !== `production`) {
  typography.injectStyles()
}

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale
