

export const websiteJsonld = (site) => {
  return {
    "@context": "https://schema.org",
    "@type": "Website",
    "publisher": {
      "@type": "Organization",
      "name": "Investdiary",
      "logo": "https://res.cloudinary.com/durunvo/image/upload/v1508520715/logo_oiubuq.png"
    },
    "url": site.siteMetadata.siteUrl,
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": site.siteMetadata.siteUrl
    },
    "description": "แบ่งปันแนวคิด ความรู้หุ้นรายตัว ประสบการณ์ การลงทุนในหุ้น"
  }
}

export const articleJsonld = (site, { slug, title, date, tags, image, description }) => {
  return {
    "@context": "https://schema.org",
    "@type": "Article",
    "publisher": {
      "@type": "Organization",
      "name": "Investdiary",
      "logo": "https://res.cloudinary.com/durunvo/image/upload/v1508520715/logo_oiubuq.png"
    },
    "author": {
      "@type": "Person",
      "name": "Investdiary",
      "image": {
        "@type": "ImageObject",
        "url": "https://res.cloudinary.com/durunvo/image/upload/v1508520715/logo_oiubuq.png",
        "width": 307,
        "height": 307
      },
      "url": site.siteMetadata.siteUrl,
      "sameAs": []
    },
    "headline": title,
    "url": `${site.siteMetadata.siteUrl}${slug}`,
    "datePublished": date,
    "dateModified": date,
    "image": image ? {
      "@type": "ImageObject",
      "url": image.publicURL,
      "width": image.childImageSharp ? image.childImageSharp.resolutions.width : 400,
      "height": image.childImageSharp ? image.childImageSharp.resolutions.height : 205
    } : null,
    "keywords": tags.join(', '),
    "description": description,
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": site.siteMetadata.siteUrl
    }
  }
}