const tags = require('./meta-tags').default

const _tags = tags.reduce((prev, current) => {
  return {
    ...prev,
    [current.name]: current.slug
  }
}, {})

exports.transformGhostPath = (path = '') => {
  if (path.match(/[0-9]{4}-[0-9]{2}-[0-9]{2}---/)) {
    const splitted = path.split('---')
    return `${splitted[0].replace(new RegExp(/-+/, 'g'), '/')}/${splitted[1]}`
  }
  return path
}

exports.getTagSlug = (tag = '') => {
  return _tags[tag]
}

exports.kebabCase = (string = '') => {
  let result = `${string}`

  // Convert camelCase capitals to kebab-case.
  result = result.replace(/([a-z][A-Z])/g, (match) => {
    return match.substr(0, 1) + '-' + match.substr(1, 1).toLowerCase()
  })

  // Convert non-camelCase capitals to lowercase.
  result = result.toLowerCase()

  // Convert non-alphanumeric characters to hyphens
  result = result.replace(/[^-a-z0-9]+/g, '-')

  // Remove hyphens from both ends
  result = result.replace(/^-+/, '').replace(/-$/, '')

  return result
}
